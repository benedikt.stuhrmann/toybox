<?php
require_once("UserManager.php");

class AjaxHandler {

  private $logger;
  private $template;

  public function __construct($template) {
    $this->logger = new Logger();
    $this->template = $template;
  }

  public function handle($function_name) {
    $function_name = str_replace("ajax_", "", $function_name);
    if (method_exists($this, $function_name)) {
      echo json_encode($this->$function_name());
    } else {
      $this->logger->log("Requested AJAX function \"" . $function_name . "\" does not exist", "Warning");
      echo "error";
    }
  }

  public function isAjaxRequest() {
    $requested_url = safe($_GET, "url");
    if ($requested_url) return strpos($requested_url, "ajax_") !== false;
    else return false;
  }

  /**
   * Tries to log in a user by an email-password combination
   * @return array : array ( "answ" => "success/no-match/error"
   *                         "menu" => the user-menu template
   *                       )
   */
  private function login() {
    $email       = safe($_POST, "email");
    $password    = safe($_POST, "password");
    $remember_me = safe($_POST, "remember_me");

    $user_manager = new UserManager();
    return $user_manager->login($email, md5($password), $remember_me == "true", $this->template);
  }

  /**
   * Logs out the current user
   * @return array : array ( "answ" => "success/error"
   *                         "menu" => the user-menu template
   *                       )
   */
  private function logout() {
    $user_manager = new UserManager();
    return $user_manager->logout($this->template);
  }

  /**
   * Creates a new user or updates an existing user
   * @return array : array ( "answ" => "success/error" )
   */
  private function createEditUser() {
    $form_data = $_POST;

    require_once(APPLICATION_ROOT . "presentation" . DS . "controllers" . DS . "Controller.php"); // base controller
    require_once(APPLICATION_ROOT . "presentation" . DS . "controllers" . DS . "UsersCreateEdit.php"); // create-edit-user controller
    $controller = new UsersCreateEditController();
    return $controller->handle($this->template, $form_data);
  } // end createEditUser

  private function createEditModule() {
    $form_data = $_POST;

    require_once(APPLICATION_ROOT . "presentation" . DS . "controllers" . DS . "Controller.php"); // base controller
    require_once(APPLICATION_ROOT . "presentation" . DS . "controllers" . DS . "Modules.php"); // modules controller
    $controller = new ModulesController();
    return $controller->handle($this->template, $form_data);
  } // end createEditModule

} // end AjaxHandler