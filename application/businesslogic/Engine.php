<?php
require_once("Smarty.class.php");
require_once("Logger.php");
require_once("AjaxHandler.php");
require_once("utils.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlHelper.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlPageDAO.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlUrlDAO.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlModuleDAO.php");

/**
 * Class Engine handles each request to the applications. Distributes requests to the correct controller
 * and renders the correct template.
 */
class Engine {

  private $logger;
  private $page_DAO;
  private $url_DAO;
  private $module_DAO;

  function __construct() {
    $this->logger = new Logger();
    $this->page_DAO = new MySqlPageDAO();
    $this->url_DAO = new MySqlUrlDAO();
    $this->module_DAO = new MySqlModuleDAO();
  }

  /**
   * Called for every request.
   * Handles it and calls the correct controller/template.
   * Also handles AJAX calls and calls the AjaxHandler.
   */
  public function handle() {
    // set up logging
    $this->logger->cleanLogs();
    $this->logger->setErrorReporting();

    // prepare smarty
    $template = $this->prepareSmarty();
    $template->assign("resources", DS . "toybox" . DS . "resources" . DS);

    // direct AJAX requests to the AJAX handler
    $ajaxHandler = new AjaxHandler($template);
    if ($ajaxHandler->isAjaxRequest()) {
      $ajaxHandler->handle($_GET["url"]);
      return;
    }

    // call controller and render template
    $filenames = $this->getFilenames();
    $this->authUser($template);
    $this->callController($template, safe($filenames, "module"), $filenames["controller_filename"], $filenames["controller_name"]);
    $this->renderTemplate($template, safe($filenames, "module"), $filenames["template_filename"], $filenames["page_title"]);
  } // end handle

  /**
   * Analyzes which controller and template should be called and returns the filenames of those.
   * @return array : an array containing following filenames: directory,
   *                                                          controller_filename,
   *                                                          controller_name,
   *                                                          template_filename,
   *                                                          page_title
   */
  private function getFilenames() {
    // set URL string of URL Model, if no URL exists, set "home" as URL code
    $url_model = new Url();
    $url = str_replace_first("/toybox/", "", safe($_SERVER, "REQUEST_URI", ""));
    if ($url !== "") $url_model->setUrl($url);
    else $url_model->setCode("home");


    // prepare page_model with available info
    $page_model = new Page();
    $page_model->setUrl($url_model);

    // get the whole page data from the database
    $page_model = $this->page_DAO->getPage($page_model);

    if (!$page_model) {
      // TODO: DELETE OR FINISH?
      /*if (strpos($url_model->getUrl(), "/module/") !== false) {
        // check for page where url contains /module/---!!!XYZ!!!---/
        $module_code = preg_match("", $url_model->getUrl());
        $this->page_DAO->getPageWhereUrlLike("/module/" + $module_code + "/");
        // call the template in that module
      }*/
      $this->logger->log("Tried to access URL that does not exist: '" . $url . "'", "Error");
      redirect("error_404", 404);
    }

    if (str_starts_with($page_model->getName(), "module_")) {
      // url is for a module, so enter the modules directory and call the index.html
      $module_code = str_replace_first("module_", "", $page_model->getName());
      $module_model = new Module();
      $module_model->setCode($module_code);

      return array(
        "module"              => $this->module_DAO->getModule($module_model),
        "controller_filename" => "Controller.php",
        "controller_name"     => "Controller",
        "template_filename"   => "index.html",
        "page_title"          => ucfirst($module_code)
      );
    } else {
      // url is for a standard page
      $parts = explode("_", $page_model->getName());
      $parts = array_map("ucfirst", $parts);
      $name = implode("", $parts);

      return array(
        "controller_filename" => $name . ".php",
        "controller_name"     => $name . "Controller",
        "template_filename"   => $page_model->getTemplate()->getCode() . ".html",
        "page_title"          => $page_model->getTitle()
      );
    }
  }

  /**
   * Sets up smarty directories and prepares a new template.
   * @param string $directory : path to the templates folder. presentation/views as default
   * @return Smarty : Returns the Smarty/template object
   */
  private function prepareSmarty($directory = APPLICATION_ROOT . "presentation" . DS . "views") {
    $template = new Smarty();

    $template->template_dir = $directory;
    $template->compile_dir  = ROOT . DS . ".." . DS . "tmp" . DS . "smarty";
    $template->config_dir   = ROOT . DS . ".." . "config" . DS . "smarty";
    $template->cache_dir    = ROOT . DS . ".." . "cache";

    return $template;
  }

  /**
   * Calls the controller with the given filename, if it exists.
   * @param Smarty $template : the smarty template that will be passed to the controller
   * @param Module $module_model: the modules model
   * @param string $filename : the filename of the controller
   * @param string $name : the name of the controller class that has to be instantiated
   */
  private function callController(Smarty $template, $module_model = null, $filename, $name) {
    if ($module_model && file_exists(APPLICATION_ROOT . "modules" . DS . $module_model->getCode() . DS . $filename)) {
      // accessing a module
      require_once(APPLICATION_ROOT . "modules" . DS . $module_model->getCode() . DS . $filename); // module controller
      $controller = new $name();
      $controller->handle($template);
      return;
    }

    // accessing any other page
    $filepath_controllers = APPLICATION_ROOT . "presentation" . DS . "controllers" . DS;
    if (file_exists($filepath_controllers . $filename)) {
      require_once($filepath_controllers . "Controller.php"); // base controller
      require_once($filepath_controllers . $filename); // specific controller
      $controller = new $name();
      $controller->handle($template);
    } else {
      $this->logger->log("Controller could not be instantiated. Controller '" . $filename . "' could not be found in '" . $filepath_controllers . "'.", "WARNING");
    }
  }

  /**
   * Renders the given template on the base of index.html.
   * @param Smarty $template : the smarty template
   * @param Module $module_model : the module_model
   * @param string $filename : the filename of the template
   * @param string $page_title : the title of the page
   */
  private function renderTemplate(Smarty $template, Module $module_model = null, $filename, $page_title) {
    // if rendering a module
    if ($module_model) {
      $template->assign("module", $module_model);
      $overlay = $template->fetch("module_overlay.html");
      $essentials = $template->fetch("essentials.html");
      $template = $this->prepareSmarty(APPLICATION_ROOT . DS . "modules" . DS . $module_model->getCode() . DS);
      $template->assign("toybox_overlay", $overlay);
      $template->assign("toybox_essentials", $essentials);
      $template->assign("module_root", DS ."toybox" . DS . "application" . DS . "modules" . DS . $module_model->getCode() . DS);
    }

    $template->assign("referrer", safe($_SERVER, "HTTP_REFERER"));
    if ($filename)   $template->assign("file", $filename);
    if ($page_title) $template->assign("title", $page_title);
    $template->display("index.html");
  }

  /**
   * Checks if a user is logged in.
   * If user is logged in, user data gets assigned to smarty.
   * @param Smarty $template : the smarty template
   */
  private function authUser(Smarty $template) {
    $user_manager = new UserManager();
    $user_model = $user_manager->getLoggedInUser($template);
    $template->assign("logged_in_user", $user_model);
  }

} // end Engine
