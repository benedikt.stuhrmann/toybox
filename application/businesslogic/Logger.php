<?php

class Logger {

  /**
   * Logs a given message.
   * @param string $message : The message to be logged
   * @param string $type : Type of the error. Inserted before log-message. ("Error" as default)
   */
  public function log($message, $type = "ERROR") {
    error_log ($type . ": " .  $message);
  }

  /**
   * Cleans the logs folder by deleting all logfiles that are older than a week and creating a new log file each day.
   * Logfiles have a filename pattern like: 2017-12-17.errors.log
   */
  public function cleanLogs() {
    $logs = opendir(ROOT . DS . "tmp" . DS . "logs" . DS);

    // remove logfiles older than $keep_days
    $keep_days = 7;
    while (($file = readdir($logs)) !== false) {
      if (in_array($file, array(".", ".."))) {
        continue;
      } else if (filectime(ROOT . DS . "tmp" . DS . "logs" . DS . $file) <= time() - $keep_days * 24 * 60 * 60) {
        unlink(ROOT . DS . "tmp" . DS . "logs" . DS . $file);
      }
    }

    // add a new logfile if there is no one for today already
    $new_file_name = date("Y-m-d") . ".errors.log";
    if (!file_exists(ROOT . DS . "tmp" . DS . "logs" . DS . $new_file_name)) {
      fopen(ROOT . DS . "tmp" . DS . "logs" . DS . $new_file_name, "w");
    }

    closedir($logs);
  }

  /**
   * Enables the error reporting and sets output for error log
   * @param boolean $report : true to enable it, false to disable
   */
  public function setErrorReporting($report = true) {
    error_reporting(E_ALL);
    ini_set("log_errors", "On");
    $todays_log = date("Y-m-d") . ".errors.log";
    ini_set("error_log", ROOT . DS . "tmp" . DS . "logs" . DS . $todays_log);

    if ($report) {
      ini_set("display_errors", "On");
    } else {
      ini_set("display_errors", "Off");
    }
  }

} // end Logger