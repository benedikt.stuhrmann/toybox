<?php
require_once("Logger.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlModuleDAO.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlUrlDAO.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlTemplateDAO.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlPageDAO.php");

class ModuleManager {

  private $logger;
  private $module_DAO;
  private $url_DAO;
  private $template_DAO;
  private $page_DAO;

  function __construct() {
    $this->logger = new Logger();
    $this->module_DAO = new MySqlModuleDAO();
    $this->url_DAO = new MySqlUrlDAO();
    $this->template_DAO = new MySqlTemplateDAO();
    $this->page_DAO = new MySqlPageDAO();
  } // end constructor

  public function getModule(Module $module_model) {
    return $this->module_DAO->getModule($module_model);
  } // end getModule

  /**
   * Gets all modules from the database
   * @return array|bool : An array of module models,
   *                      false, if something failed
   */
  public function getModules() {
    return $this->module_DAO->getModules();
  } // end getModules

  /**
   * Updates an existing module with the given id. If no $update id is given and module model does not have a module id, a new module gets created
   * @param Module $module_model : the module-data for the update or creation
   * @param mixed $update : the modules id, that should be updated. If none is given, a new module gets created
   * @return bool
   */
  public function createUpdateModule(Module $module_model, $url, $update = false) {
    if (!$update) {
      // for creation of new module, check if all required data is there
      if (!$module_model->getName()) return false;
      if (!$module_model->getCode()) return false;
      if (!$url) return false;
      if ($module_model->getVisibility() === null) return false;
    }

    // check if module name already exists
    if ($module_model->getName() && $this->isUnique("name", $module_model->getName()) !== true) return "module name is not unique";
    // check if code already exists
    if ($module_model->getCode() && $this->isUnique("code", $module_model->getCode()) !== true) return "code is not unique";
    // check if url already exists
    if ($url && $this->isUnique("url", $url) !== true) return "url is not unique";

    if ($update) {
      // update existing module
      $module_model->setId($update);
      $result = $this->module_DAO->updateModule($module_model);
      return $result;
    } else {
      $page_model = new Page();

      $page_model->setName("module_" . $module_model->getCode());

      $template_model = new Template();
      $template_model->setCode("module_index");
      $template_model = $this->template_DAO->getTemplate($template_model);
      $page_model->setTemplate($template_model);

      $url_model = new Url();
      $url_model->setCode("module_" . $module_model->getCode());
      $url_model->setUrl("module/" . $url . "/");
      $page_model->setUrl($url_model);

      $page_model->setTitle($module_model->getName());

      if (!$this->page_DAO->insertPage($page_model)) return false;
      // insert module into database
      return $this->module_DAO->insertModule($module_model);
    }
  } // end createUpdateModule

  public function createUpdateModuleStructure(Module $module_model, $thumbnail = false) {
    $base_module_dir = ROOT . DS . "application" . DS . "modules" . DS . "base_module";
    $module_dir = ROOT . DS . "application" . DS . "modules" . DS . $module_model->getCode();
    $thumbnail_dir = ROOT . DS . "public" . DS . "resources" . DS . "images" . DS . "thumbnails";

    // create folder
    if (!file_exists($module_dir)) mkdir($module_dir, 0777, true);
    // create Controller.php
    if (!file_exists($module_dir . DS . "Controller.php"))
      copy($base_module_dir . DS . "Controller.php", $module_dir . DS . "Controller.php");
    // create index.html
    if (!file_exists($module_dir . DS . "index.html"))
      copy($base_module_dir . DS . "index.html", $module_dir . DS . "index.html");
    // create styles.scss
    if (!file_exists($module_dir . DS . "styles.scss"))
      copy($base_module_dir . DS . "styles.scss", $module_dir . DS . "styles.scss");
    // create thumbnail
    if ($thumbnail) {
      // use custom uploaded thumbnail if exists
      $success = file_put_contents($thumbnail_dir . DS . $module_model->getCode() . ".png", $thumbnail);
      if (!$success) {
        $this->logger->log("Could not create thumbnail for module " . $module_model->getCode() . ".");
        return false;
      }
    } else {
      // use default thumbnail
      if (!file_exists($thumbnail_dir . DS . $module_model->getCode() . ".png")) {
        if (!copy($base_module_dir . DS . "thumbnail.png", $thumbnail_dir . DS . $module_model->getCode() . ".png"))
          $this->logger->log("Could not create thumbnail for module " . $module_model->getCode() . ".");
      }
    }

    return true;
  } // createUpdateModuleStructure

  /**
   * @param $key : name of the unique attribute ("name", "code", "url")
   * @param $value : the value of the attribute
   * @return bool|string : true for unique,
   *                       false for non-unique.
   *                       "error" when a parameter is missing,
   *                       "unknown unique attribute" when $key is not known
   */
  private function isUnique($key, $value) {
    if (!isset($key) || !isset($value)) return "error";

    switch($key) {
      case "name" :
        $module_model = new Module();
        $module_model->setName($value);
        $result = $this->module_DAO->getModule($module_model);
        break;
      case "code" :
        $module_model = new Module();
        $module_model->setCode($value);
        $result = $this->module_DAO->getModule($module_model);
        break;
      case "url" :
        $url_model = new Url();
        $url_model->setUrl($value);
        $result = $this->url_DAO->getUrl($url_model);
        break;
      default : return "unknown unique attribute";
    }

    if ($result) return false;
    else return true;
  } // end isUnique

} // end ModuleManager