<?php
require_once("Logger.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlUserDAO.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlLoggedInUserDAO.php");

class UserManager {

  private $logger;
  private $user_DAO;
  private $logged_in_user_DAO;

  function __construct() {
    $this->logger = new Logger();
    $this->user_DAO = new MySqlUserDAO();
    $this->logged_in_user_DAO = new MySqlLoggedInUserDAO();
  } // end constructor

  public function login($email, $password, $remember_be, $template) {
    // check if user with email-password combination exists
    $user_model = new User();
    $user_model->setEmail($email);
    $user_model->setPassword($password);

    $user_model = $this->user_DAO->getUser($user_model);
    if (!$user_model) return array("answ" => "no-match");

    // valid user data -> log in the user
    if (!session_start()) return array("answ" => "error");
    $this->registerUserAction($user_model);

    $logged_in_user_model = new LoggedInUser();
    $logged_in_user_model->setUserId($user_model->getId());
    $logged_in_user_model->setLoggedInFrom($_SERVER['REMOTE_ADDR']);
    $logged_in_user_model->setUserAgent($_SERVER['HTTP_USER_AGENT']);

    // check if there is a logged_in user entry without a remember_me token that did not time out yet.
    // If so ... delete it
    $exists = $this->logged_in_user_DAO->getLoggedInUser($logged_in_user_model);
    if ($exists && $exists->getRememberMeToken() == null) $this->logged_in_user_DAO->deleteLoggedInUser($exists->getId());

    $logged_in_user_model->setSessionId(session_id());
    $logged_in_user_model->setLoggedInAt(date("Y:m:d H:i:s"));
    if ($remember_be) { // if user checked remember me option
      $token = generateToken(50);
      $logged_in_user_model->setRememberMeToken($token);
      setcookie("remember_me", $token, time()+60*60*24*30); // expires after 30 days
    }

    $result = $this->logged_in_user_DAO->insertLoggedInUser($logged_in_user_model);
    if ($result === "error") return array("answ" => $result);

    $_SESSION["user_id"] = $user_model->getId(); // for quick and easy access
    $_SESSION["user"] = $user_model;

    $template->assign("logged_in_user", $user_model);
    $user_menu = $template->fetch("user_menu.html");

    return array(
      "answ" => "success"
    , "menu" => $user_menu
    );
  } // end login

  public function logout($template) {
    $session_id = safe($_COOKIE, "PHPSESSID");
    session_id($session_id);
    session_start();
    $user_model = $_SESSION["user"];
    if (!$user_model) {
      $this->logger->log("User tried to log out but was already logged out.");
      return array("answ" => "error");
    }
    if (!session_destroy()) {
      // error
      $this->logger->log("Could not log out user");
      return array("answ" => "error");
    } else {
      // user got logged out
      $this->registerUserAction($user_model);
      setcookie("remember_me", "", time()-3600); // delete remember_me token

      // remove logged in user model
      $logged_in_user_model = new LoggedInUser();
      $logged_in_user_model->setUserId($user_model->getId());
      $logged_in_user_model->setLoggedInFrom($_SERVER['REMOTE_ADDR']);
      $logged_in_user_model->setUserAgent($_SERVER['HTTP_USER_AGENT']);
      $logged_in_user_model = $this->logged_in_user_DAO->getLoggedInUser($logged_in_user_model);
      $this->logged_in_user_DAO->deleteLoggedInUser($logged_in_user_model->getId());

      return array(
        "answ" => "success"
      );
    }
  } // end logout

  public function getUser(User $user_model) {
    return $this->user_DAO->getUser($user_model);
  } // end getUser

  /**
   * Gets all users from the database
   * @return array|bool : An array of user models,
   *                      false, if something failed
   */
  public function getUsers() {
    return $this->user_DAO->getUsers();
  } // end getUsers

  /**
   * Checks if a user is logged in and returns its user model.
   * Otherwise returns null.
   * @param Smarty $template : the smarty template
   * @return User|null : user model, if a user is logged in,
   *                     null otherwise
   */
  public function getLoggedInUser(Smarty $template) {
    $session_id = safe($_COOKIE, "PHPSESSID");
    if ($session_id && !isset($_SESSION)) {
      session_id($session_id);
      session_start();
    } elseif (!$session_id) {
      // check if there is a remember_me token
      if (!isset($_COOKIE["remember_me"])) return null;

      // if so, check if it fits a db entry together with IP and user agent
      $logged_in_user_model = new LoggedInUser();
      $logged_in_user_model->setLoggedInFrom($_SERVER['REMOTE_ADDR']);
      $logged_in_user_model->setUserAgent($_SERVER['HTTP_USER_AGENT']);
      $logged_in_user_model->setRememberMeToken($_COOKIE["remember_me"]);

      $logged_in_user_model = $this->logged_in_user_DAO->getLoggedInUser($logged_in_user_model);
      if (!$logged_in_user_model) return null;

      // get the user model
      $user_model = new User();
      $user_model->setId($logged_in_user_model->getUserId());
      $user_model = $this->user_DAO->getUser($user_model);

      // delete logged in user entry and auto log in the user (creates a new logged in user entry with a new remember_me token)
      $this->logged_in_user_DAO->deleteLoggedInUser($logged_in_user_model->getId());
      $this->login($user_model->getEmail(), $user_model->getPassword(), true, $template);
    }

    return safe($_SESSION, "user");
  } // end getLoggedInUser

  /**
   * Updates an existing user with the given id. If no $update id is given and user model does not have an user id, a new user gets created
   * @param User $user_model : the userdata for the update or creation
   * @param mixed $update : the users id, that should be updated. If none is given, a new user gets created
   * @return bool
   */
  public function createUpdateUser(User $user_model, $update = false) {
    if (!$update) {
      // for creation of new user, check if all required data is there
      if (!$user_model->getUsername()) return false;
      if (!$user_model->getFirstName()) return false;
      if (!$user_model->getLastName()) return false;
      if (!$user_model->getEmail()) return false;
      if (!$user_model->getPassword()) return false;
    }

    // check if username already exists
    if ($user_model->getUsername() && $this->isUnique("username", $user_model->getUsername()) !== true) return "username is not unique";
    // check if email already exists
    if ($user_model->getEmail() && $this->isUnique("email", $user_model->getEmail()) !== true) return "email is not unique";

    if ($update) {
      // update existing user
      $user_model->setId($update);
      $result = $this->user_DAO->updateUser($user_model);

      // if the logged in user got updated, refresh user data in session
      if ($result && safe($_SESSION, "user_id") == $user_model->getId()) {
        $updated_user_model = $this->getUser($user_model);
        $_SESSION["user"] = $updated_user_model;
      }

      return $result;
    } else {
      // insert user into database
      return $this->user_DAO->insertUser($user_model);
    }
  } // end createUpdateUser

  /**
   * Registers a user action and updates its last seen value.
   * @param User $user_model : the user to be updated
   */
  private function registerUserAction(User $user_model) {
    $user_model->setLastSeen(date("Y:m:d H:i:s"));
    $this->user_DAO->updateUser($user_model);
  } // end registerUserAction


  /**
   * @param $key : name of the unique attribute ("username", "email")
   * @param $value : the value of the attribute
   * @return bool|string : true for unique,
   *                       false for non-unique.
   *                       "error" when a parameter is missing,
   *                       "unknown unique attribute" when $key is not known
   */
  private function isUnique($key, $value) {
    if (!isset($key) || !isset($value)) return "error";

    $user_model = new User();

    switch($key) {
      case "username" : $user_model->setUsername($value);
        break;
      case "email" : $user_model->setEmail($value);
        break;
      default : return "unknown unique attribute";
    }

    $result = $this->user_DAO->getUser($user_model);
    if ($result) return false;
    else return true;
  } // end isUnique

} // end UserManager