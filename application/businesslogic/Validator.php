<?php

class Validator {

  private $language;
  private $inputs = array();
  private $validated = false;
  private $errors;
  private $error_messages = array (
    "en" => array (
      "error" => "An error occured."
    , "unknown" => "Unknown constraint."
    , "required" => "This value is required."
    , "minlength" => "This value is to short. It has to be between ? and ? characters long."
    , "maxlength" => "This value is to long. It has to be between ? and ? characters long."
    , "match" => "? does not equal ?."
    , "email" => "'?' is not a valid email address."
    , "email2" => "'?' should not be an email address."
    , "password" => "Invalid password."
    , "password-lowerCase" => "Should contain at least one lowercase letter."
    , "password-upperCase" => "Should contain at least one uppercase letter."
    , "password-number" => "Should contain at least one number."
    , "password-specialChar" => "Should contain at least one special character."
    , "module_code" => "Invalid module code."
    , "module_code-specialChar" => "Must not contain any special characters except '_'."
    , "module_code-upperCase" => "Must not contain any upper case letters."
    , "module_code-startsWith" => "Must not start with '_'."
    , "module_code-endsWith" => "Must not end with '_'."
    ),
    "de" => array (
      "error" => "Ein Fehler ist aufgetreten."
    , "unknown" => "Unbekannter Validator."
    , "required" => "Dies ist ein Pflichtfeld."
    , "minlength" => "Diese Eingabe ist zu kurz. Sie muss zwischen ? und ? Zeichen lang sein."
    , "maxlength" => "Diese Eingabe ist zu lang. Sie muss zwischen ? und ? Zeichen lang sein."
    , "match" => "'?' stimmt nicht mit '?' überein."
    , "email" => "'?' ist keine korrekte E-Mail Adresse."
    , "email2" => "'?' darf keine E-Mail Adresse sein."
    , "password" => "Ungültiges Passwort."
    , "password-lowerCase" => "Sollte mindestens einen Kleinbuchstaben enthalten."
    , "password-upperCase" => "Sollte mindestens einen Großbuchstaben enthalten."
    , "password-number" => "Sollte mindestens eine Zahl enthalten."
    , "password-specialChar" => "Sollte mindestens ein Sonderzeichen enthalten."
    , "module_code" => "Ungültiger Modulcode."
    , "module_code-specialChar" => "Darf keine Sonderzeichen, außer '_', enthalten."
    , "module_code-upperCase" => "Darf keine Großbuchstaben enthalten."
    , "module_code-startsWith" => "Darf nicht mit '_' beginnen."
    , "module_code-endsWith" => "Darf nicht mit '_' enden."
    )
  );

  /**
   * Validator constructor.
   * @param string $lang : Language of the error messages. "de" for german and "en" for english
   */
  public function __construct($lang = "de") {
    $this->language = $lang;
  } // end constructor

  /**
   * Adds a new input to the validator.
   * @param $name : the name of the input
   * @param $data : the value of the input
   * @param $constraints : an array of constraints with constraint_name => constraint_value pairs
   *                       Example: array ( "required" => true
   *                                      , "minlength" => 5
   *                                      , "match" => "Hello World"
   *                                      )
   *
   *  Possible params for constraint_name : required, minlength, maxlength, match, email, password
   */
  public function add($name, $data, $constraints) {
    $this->inputs[$name] = array ( "data"        => $data
                                 , "constraints" => $constraints);
  } // end add

  /**
   * Validates all inputs.
   * @return bool : true, if all inputs match their constraints
   *                false, if at least one input is invalid
   */
  public function validate() {
    $this->validated = true;

    foreach ($this->inputs as $name => $input) {
      foreach ($input["constraints"] as $constraint_name => $constraint_value) {
        $validatorFunction = "check" . ucfirst($constraint_name);
        if (!method_exists($this, $validatorFunction)) {
          $this->errors[$name][$constraint_name] = $this->getErrorMessage("unknown");
          continue;
        }

        $validationResult = $this->$validatorFunction($input["data"], $constraint_value);
        if ($validationResult !== true) {
          $this->errors[$name][$constraint_name] = $validationResult;
        }
      } // end foreach constraints
    } // end foreach inputs

    return !isset($this->errors);
  } // end validate

  /**
   * Returns an array of errors. Validate has to be called before.
   * @return mixed : Array with an index for each invalid input. Key of the index equals the inputs name.
   *                 String that informs you to call validate() before
   */
  public function getErrors() {
    // check if validate() was called beforehand.
    if (!$this->validated) return "Make sure to call validate() before trying to get the errors.";

    return $this->errors;
  } // end getErrors

  /**
   * Returns an error message for the given key in the validators language
   * @param string $key : the key of the error message
   * @param array $data : data that gets inserted into placeholders
   * @return string : Error Message as a string
   */
  private function getErrorMessage($key = "error", $data = array()) {
    $message = $this->error_messages[$this->language][$key];
    $placeholder_count = substr_count($message, "?");
    for ($i=0; $i<$placeholder_count; $i++) {
      $message = str_replace_first("?", $data[$i], $message);
    }
    return $message;
  } // end getErrorMessage

  /**
   * Checks if the given data exists
   * @param mixed $data : the inputs data, that will be checked
   * @param bool $value : either true (required) or false (not required)
   * @return bool|string : true, if required value exists
   *                       string with error message, if required value is missing
   */
  private function checkRequired($data, $value = true) {
    // if it is required
    if ($value) {
      $valid = isset($data) && $data !== "";
      if ($valid) return true;
      else return $this->getErrorMessage("required");
    }
    // if it is not required
    else return true;
  } // end checkRequired

  /**
   * Checks if giving string is min $value characters long
   * @param string $data : the inputs data, that will be checked
   * @param int $value : minimum length of the string
   * @return bool|string : true, if $data is at least $value characters long
   *                       string with error message, if $data is more than $value characters long
   */
  private function checkMinlength($data, $value = 0) {
    $valid = strlen($data) >= $value;
    if ($valid) return true;
    else return $this->getErrorMessage("minlength", array(strlen($data), $value));
  } // end checkMinlength

  /**
   * Checks if giving string is max $value characters long
   * @param string $data : the inputs data, that will be checked
   * @param int $value : maximum length of the string
   * @return bool|string : true, if $data is shorter than $value characters
   *                       string with error message, if $data is longer than $value characters
   */
  private function checkMaxlength($data, $value = 0) {
    $valid = strlen($data) <= $value;
    if ($valid) return true;
    else return $this->getErrorMessage("maxlength", array(strlen($data), $value));
  } // end checkMaxlength

  /**
   * Compares two values ($data and $value) and checks if they are equal
   * @param mixed $data : the inputs data, that will be checked
   * @param mixed $value : the value that will be compared with $data
   * @return bool|string : true, if $data equals $value
   *                       string with error message, if $data does not equal $value
   */
  private function checkMatch($data, $value = "") {
    $valid = $data === $value;
    if ($valid) return true;
    else return $this->getErrorMessage("match", array($data, $value));
  } // end checkMatch

  /**
   * Checks if $data is an email address.
   * @param string $data : the inputs data, that will be checked
   * @param bool $value : true, check if email
   *                      false, check if not email
   * @return bool|string : true, if $value is true and $data is an email address
   *                       true, if $value is false and $data is no email address
   *                       string with error message, if $value is true and $data is no email address
   *                       string with error message, if $value is false and $data is an email address
   */
  private function checkEmail($data, $value = true) {
    $valid = filter_var($data, FILTER_VALIDATE_EMAIL);
    if ($value) {
      // should be an email
      if ($valid) return true;
      else return $this->getErrorMessage("email", array($data));
    } else {
      // should not be an email
      if (!$valid) return true;
      else return $this->getErrorMessage("email2", array($data));
    }
  } // end checkEmail

  /**
   * Checks if $data is a valid password.
   * Has to be at least 6 characters long, contain an number, a special character and at least one upper case and lower case letter
   * @param string $data : the string to be checked
   * @param bool $value : true or false
   * @return bool|string : true, if $data is a valid password
   *                       string with error message, if $data is no valid password
   */
  private function checkPassword($data, $value = true) {
     $reason = "";

    // check if password is long enough
    $result = $this->checkMinlength($data, 6);
    if ($result !== true) $reason .= "<br/>" . $result;
    // check if it contains a lower case letter
    if (!preg_match("/[a-z]/", $data)) $reason .= "<br/>" . $this->getErrorMessage("password-lowerCase");
    // check if it contains an upper case letter
    if (!preg_match("/[A-Z]/", $data)) $reason .= "<br/>" . $this->getErrorMessage("password-upperCase");
    // check if it contains at least one number
    if (!preg_match("/\\d/", $data)) $reason .= "<br/>" . $this->getErrorMessage("password-number");
    // check if it contains at least one special character
    if (!preg_match("/[\'\"^£$%&*()}{@#~?><>,\/|=_+¬-]/", $data)) $reason .= "<br/>" . $this->getErrorMessage("password-specialChar");

    if ($reason === "") return true;
    else return $this->getErrorMessage("password") . $reason;
  } // end checkPassword

  /**
   * Checks if $data is a valid module code, containing no special characters except "_" (not on start or end),
   * and has only lowercase letters.
   * @param $data : Module code to be checked
   * @param boolean $value : true or false
   * @return bool|string   : true, if code is valid,
   *                         string with error message, if $data is no valid module code
   */
  private function checkModulecode($data, $value = true) {
    $reason = "";

    // check if it contains any special characters
    if (preg_match("/[\'\"^£$%&*()}{@#~?><>,\/|=+¬-]/", $data)) $reason .= "<br/>" . $this->getErrorMessage("module_code-specialChar");
    // check if only lower case letters are used
    if (preg_match("/[A-Z]/", $data)) $reason .= "<br/>" . $this->getErrorMessage("module_code-upperCase");
    // check if it starts with "_"
    if (strpos($data, "_") === 0) $reason .= "<br/>" . $this->getErrorMessage("module_code-startsWith");
    // check if it ends with "_"
    if (strpos($data, "_") === strlen($data)) $reason .= "<br/>" . $this->getErrorMessage("module_code-endsWith");

    if ($reason === "") return true;
    else return $this->getErrorMessage("module_code") . $reason;
  } // end checkModuleCode

} // end Validator