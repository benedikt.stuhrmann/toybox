<?php

/**
 * Checks if the given key exists within the array.
 * Returns the value of the key, if it exists.
 * Returns the default value otherwise (defaults to null)
 * @param array $array : the array
 * @param $key : key to search for in the array
 * @param mixed $default : if array key does not exist, specified $default value will be returned
 * @return mixed : the value of the array index. $default if key does not exist
 */
function safe($array, $key, $default = null) {
  if (isset($array) && array_key_exists($key, $array)) return $array[$key];
  else return $default;
} // end safe

/**
* Redirects to the given URL
* @param string $url_code : URL code that user will be redirected to (default: "home")
* @param int $status_code : Status Code for the redirect (default: 303)
*/
function redirect($url_code = "home", $status_code = 303) {
  $url_model = new Url();
  $url_model->setCode($url_code);

  $url_DAO = new MySqlUrlDAO();
  $url_model = $url_DAO->getUrl($url_model);

  $url = "http://localhost:8080/toybox";
  if ($url_model) $url .= "/" . $url_model->getUrl();

  http_response_code ($status_code);
  header("Location: " . $url, true, 303);
  die();
} // end redirect

/**
 * Generates a token with the specified length
 * @param int $length : the length of the token (default = 20)
 * @return string : generated token
 */
function generateToken($length = 20) {
  $bytes = openssl_random_pseudo_bytes($length);
  return bin2hex($bytes);
} // end generateToken

/**
 * Replaces the first occurence of $search in $subject with $replace.
 * @param String $search : The string that gets replaced
 * @param String $replace : The string that acts as the replacement
 * @param String $subject : The string in which to search
 * @return String : the string
 */
function str_replace_first($search = "", $replace = "", $subject = "") {
  $search = '/'.preg_quote($search, '/').'/';
  return preg_replace($search, $replace, $subject, 1);
} // end str_replace_first

/**
 * Checks if a string starts with a specific string
 * @param $string : the string to be checked
 * @param $check_for : the string to be checked for in the beginning of $string
 * @return bool : true, if $string begins with $check_for,
 *                false otherwise
 */
function str_starts_with($string, $check_for) {
  return $check_for === substr($string, 0, strlen($check_for));
} // end str_starts_with