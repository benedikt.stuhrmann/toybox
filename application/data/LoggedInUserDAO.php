<?php

interface LoggedInUserDAO {

  /**
   * Searches for all logged in users in the database, that match with the existing given data of the logged in user model.
   * @param LoggedInUser $logged_in_user_model : A logged in user model
   * @return mixed : A logged in user model overwritten with data from the matching database entry.
   *                 If there are multiple matches, an array of logged in user models will be returned.
   *                 false, if no match was found in the database,
   *                 "error", if something went wrong
   */
  public function getLoggedInUser(LoggedInUser $logged_in_user_model);

  /**
   * Creates a new logged in user in the database,
   * @param LoggedInUser $logged_in_user_model : The logged in user model, that gets inserted into the database
   * @return boolean : true, if a new logged in user was created in the database,
   *                   false, if something went wrong
   */
  public function insertLoggedInUser(LoggedInUser $logged_in_user_model);

  /**
   * Updates an exiting logged in user in the database.
   * LoggedInUser is selected by the id contained in the logged in user model.
   * @param LoggedInUser $logged_in_user_model : The logged in user model whose id decides, which logged in user will get updated.
   *                                   Data in the database that differs from the given model, will be overridden with the models data.
   * @return boolean : true, if logged in user was successfully updated
   *                   false, if something went wrong
   */
  public function updateLoggedInUser(LoggedInUser $logged_in_user_model);

  /**
   * Deletes a logged in user by its id from the database.
   * @param $logged_in_user_id : the logged in users id
   * @return boolean : true, if logged in user was successfully deleted
   *                   false, if something went wrong
   */
  public function deleteLoggedInUser($logged_in_user_id);

} // end LoggedInUserDAO