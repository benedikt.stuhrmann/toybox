<?php

interface ModuleDAO {

  /**
   * Searches for all Modules in the database, that match with the existing given data of the module model.
   * @param Module $module_model : A module model
   * @return mixed : A module model overwritten with data from the matching database entry.
   *                 If there are multiple matches, an array of module models will be returned.
   *                 false, if no match was found in the database,
   *                 "error", if something went wrong
   */
  public function getModule(Module $module_model);

  /**
   * Gets all modules from the database.
   * @return mixed : Array of module models will be returned.
   *                 false, if no modules in the database,
   *                 "error", if something went wrong
   */
  public function getModules();

  /**
   * Creates a new module in the database,
   * @param Module $module_model: The module model, that gets inserted into the database
   * @return boolean : true, if a new module was created in the database,
   *                   false, if something went wrong
   */
  public function insertModule(Module $module_model);

  /**
   * Updates an exiting module in the database.
   * Module is selected by the id contained in the module model.
   * @param Module $module_model: The module model whose id decides, which module will get updated.
   *                          Data in the database that differs from the given model, will be overridden with the models data.
   * @return boolean : true, if module was successfully updated
   *                   false, if something went wrong
   */
  public function updateModule(Module $module_model);

  /**
   * Deletes a module by its id from the database.
   * @param $module_id : the modules id
   * @return boolean : true, if module was successfully deleted
   *                   false, if something went wrong
   */
  public function deleteModule($module_id);

} // end ModuleDAO