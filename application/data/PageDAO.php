<?php

interface PageDAO {

  /**
   * Searches for all Pages in the database, that match with the existing given data of the page model.
   * @param Page $page_model : A page model
   * @return mixed : A page model overwritten with data from the matching database entry.
   *                 If there are multiple matches, an array of page models will be returned.
   *                 false, if no match was found in the database,
   *                 "error", if something went wrong
   */
  public function getPage(Page $page_model);

  /**
   * Creates a new page in the database,
   * @param Page $page_model: The page model, that gets inserted into the database
   * @return boolean : true, if a new page was created in the database,
   *                   false, if something went wrong
   */
  public function insertPage(Page $page_model);

  /**
   * Updates an exiting page in the database.
   * Page is selected by the id contained in the page model.
   * @param Page $page_model: The page model whose id decides, which page will get updated.
   *                          Data in the database that differs from the given model, will be overridden with the models data.
   * @return boolean : true, if page was successfully updated
   *                   false, if something went wrong
   */
  public function updatePage(Page $page_model);

  /**
   * Deletes a page by its id from the database.
   * @param $page_id : the pages id
   * @return boolean : true, if page was successfully deleted
   *                   false, if something went wrong
   */
  public function deletePage($page_id);

} // end PageDAO