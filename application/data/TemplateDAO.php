<?php

interface TemplateDAO {

  /**
   * Searches for all templates in the database, that match with the existing given data of the template model.
   * @param Template $template_model : A template model
   * @return mixed : A template model overwritten with data from the matching database entry.
   *                 If there are multiple matches, an array of template models will be returned.
   *                 false, if no match was found in the database,
   *                 "error", if something went wrong
   */
  public function getTemplate(Template $template_model);

  /**
   * Creates a new template in the database,
   * @param Template $template_model : The template model, that gets inserted into the database
   * @return boolean : true, if a new template was created in the database,
   *                   false, if something went wrong
   */
  public function insertTemplate(Template $template_model);

  /**
   * Updates an exiting template in the database.
   * Template is selected by the id contained in the template model.
   * @param Template $template_model : The template model whose id decides, which template will get updated.
   *                                   Data in the database that differs from the given model, will be overridden with the models data.
   * @return boolean : true, if template was successfully updated
   *                   false, if something went wrong
   */
  public function updateTemplate(Template $template_model);

  /**
   * Deletes a template by its id from the database.
   * @param $template_id : the templates id
   * @return boolean : true, if template was successfully deleted
   *                   false, if something went wrong
   */
  public function deleteTemplate($template_id);

} // end TemplateDAO