<?php

interface UrlDAO {

  /**
   * Searches for all URLs in the database, that match with the existing given data of the URL model.
   * @param Url $url_model : An url model
   * @return mixed : An Url Model overwritten with data from the matching database entry.
   *                 If there are multiple matches, an array of Url Models will be returned.
   *                 false, if no match was found in the database,
   *                 "error", if something went wrong
   */
  public function getUrl(Url $url_model);

  /**
   * Creates a new url in the database,
   * @param Url $url_model: The url model, that gets inserted into the database
   * @return boolean : true, if a new url was created in the database,
   *                   false, if something went wrong
   */
  public function insertUrl(Url $url_model);

  /**
   * Updates an exiting url in the database.
   * Url is selected by the id contained in the url model.
   * @param Url $url_model: The url model whose id decides, which url will get updated.
   *                        Data in the database that differs from the given model, will be overridden with the models data.
   * @return boolean : true, if url was successfully updated
   *                   false, if something went wrong
   */
  public function updateUrl(Url $url_model);

  /**
   * Deletes a url by its id from the database.
   * @param $url_id : the urls id
   * @return boolean : true, if url was successfully deleted
   *                   false, if something went wrong
   */
  public function deleteUrl($url_id);

} // end UrlDAO