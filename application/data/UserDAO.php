<?php

interface UserDAO {

  /**
   * Searches for all users in the database, that match with the existing given data of the user model.
   * @param User $user_model : An user model
   * @return mixed : An user model overwritten with data from the matching database entry.
   *                 If there are multiple matches, an array of user models will be returned.
   *                 false, if no match was found in the database,
   *                 "error", if something went wrong
   */
  public function getUser(User $user_model);

  /**
   * Creates a new user in the database,
   * @param User $user_model : The user model, that gets inserted into the database
   * @return boolean : true, if a new user was created in the database,
   *                   false, if something went wrong
   */
  public function insertUser(User $user_model);

  /**
   * Updates an exiting user in the database.
   * User is selected by the id contained in the User model.
   * @param User $user_model : The user model whose id decides, which user will get updated.
   *                           Data in the database that differs from the given model, will be overridden with the models data.
   * @return boolean : true, if user was successfully updated
   *                   false, if something went wrong
   */
  public function updateUser(User $user_model);

  /**
   * Deletes an user by its id from the database.
   * @param $user_id : the users id
   * @return boolean : true, if user was successfully deleted
   *                   false, if something went wrong
   */
  public function deleteUser($user_id);

} // end UserDAO