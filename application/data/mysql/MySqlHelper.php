<?php

class MySqlHelper {

  private $servername = "localhost";
  private $username = "root";
  private $password = "";
  private $databasename = "toybox";
  private $connection;

  /**
   * Opens a new connection to the MySQL Database
   */
  public function openConnection() {
    if ($this->connection !== null) return;

    $this->connection = new mysqli($this->servername, $this->username, $this->password, $this->databasename);
    if ($this->connection->connect_error) die("Connection failed: " . $this->connection->connect_error);
  }

  /**
   * Closes the connection to the MySQL Database
   */
  public function closeConnection() {
    $this->connection->close();
  }

  /**
   * @param String $sql : SQL query to be executed
   * @return mixed : The result of the query.
   */
  public function query($sql) {
    return $this->connection->query($sql);
  }

  /**
   * @param String $sql : SQL query to be executed
   * @param mixed $data : Data for the placeholders. Has to be an array, if there are multiple placeholders
   * @return mixed : The result of the query. An array if there are multiple results
   */
  public function preparedStatement($sql, $data) {
    $stmt = $this->connection->prepare($sql);
    if (!$stmt) return $stmt->error;

    $placeholder_count = substr_count($sql, "?");
    $data_count = count($data);
    if ($data_count < $placeholder_count) return false;

    $bind = "";
    foreach ($data as $placeholder) {
      if (is_string($placeholder) || $placeholder === null) $bind .= "s";
      if (is_int($placeholder)) $bind .= "i";
      if (is_double($placeholder)) $bind .= "d";
    }

    $stmt->bind_param($bind, ...$data);
    if (!$stmt) return $stmt->error;

    $result = $stmt->execute();

    if (!$result) return $stmt->error;
    elseif (strpos($sql, "INSERT") !== false
         || strpos($sql, "DELETE") !== false
         || strpos($sql, "UPDATE") !== false) {
      return true;
    }
    $result = $stmt->get_result();
    if (!$result) return $stmt->error;

    if ($result->num_rows <= 1) return $result->fetch_assoc();

    $rows = array();
    while($row = $result->fetch_assoc()) {
      $rows[] = $row;
    }
    return $rows;
  } // end preparedStatement

  /**
   * Inserts an entry into the database.
   * @param String $table : the database table the entry gets inserted into
   * @param Model $model : the data for the insert. Id will be generated automatically
   * @return bool|mixed
   */
  public function insertDatabaseEntry($table, Model $model) {
    if (!$table || !$model) return false;

    $fields = "";
    $values = "";
    $params = array();
    $iteration = 1;
    foreach ($model->getProperties() as $property => $value) {
      if ($property === "id") continue;
      if ($iteration > 1) {
        $fields .= ", ";
        $values .= ", ";
      }
      // if the property is a model, use its id
      if (is_object($value)) {
        $value = $value->getId();
        $property = $property . "_id";
      }

      $fields .= $property;
      $values .= "?";
      $params[] = $value;

      $iteration++;
    }

    $sql = "INSERT INTO " . $table . " (" . $fields . ") VALUES (" . $values . ")";

    $result = $this->preparedStatement($sql, $params);
    if ($result !== true) return $result;

    return true;
  } // end insertDatabaseEntry

  /**
   * Updates the specified entry in the database.
   * @param String $table : the database table to be updated
   * @param int $update_id : the entries id that should be updated
   * @param Model $model : the data for the update. Data in the database will be overwritten by data from the model
   * @return bool|mixed
   */
  public function updateDatabaseEntry($table, $update_id, Model $model) {
    if (!$table || !$update_id || !$model) return false;

    $update = "UPDATE " . $table;
    $set = " SET";
    $where = " WHERE id = ?";
    $params = array();

    $iteration = 1;
    foreach ($model->getProperties() as $property => $value) {
      if ($value === null) continue;
      if ($iteration > 1) $set .= " ,";
      if ($property === "id") {
        $iteration++;
        continue;
      }

      $set .= " " . $property . " = ?";
      $params[] = $value;

      $iteration ++;
    }

    // if no values were set, return false
    if ($iteration == 1) return false;

    $params[] = $model->getId();
    $sql = $update . rtrim($set, ",") . $where;
    $result = $this->preparedStatement($sql, $params);
    if (!$result) return false;
    else return $result;
  } // end updateDatabaseEntry

  /**
   * Deletes an entry from the database.
   * @param String $table : the database table the entry gets deleted from
   * @param int $id : id of the entry to be deleted
   * @return bool|mixed
   */
  public function deleteDatabaseEntry($table, $id) {
    if (!$table || !$id) return false;

    $sql = "DELETE FROM " . $table . " WHERE id = ?";
    $params = array($id);

    return $this->preparedStatement($sql, $params);
  } // end deleteDatabaseEntry

} // end MySqlHelper