<?php
require_once(APPLICATION_ROOT . "data" . DS . "LoggedInUserDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "LoggedInUser.php");

class MySqlLoggedInUserDAO implements LoggedInUserDAO {

  private $db;
  private $logger;

  function __construct() {
    $this->db = new MySqlHelper();
    $this->db->openConnection();
    $this->logger = new Logger();
  }

  public function getLoggedInUser(LoggedInUser $logged_in_user_model) {
    if (!$logged_in_user_model) return false;

    $logged_in_user__data = $logged_in_user_model->getProperties();
    $sql = "SELECT * FROM logged_in_user WHERE";
    $params = array();

    $iteration = 1;
    foreach ($logged_in_user__data as $property => $value) {
      if ($value === null) continue;
      if ($iteration != 1) $sql .= " AND";

      $sql .= " " . $property . " = ?";
      $params[] = $value;

      $iteration ++;
    }

    // if no values were set, return false
    if ($iteration == 1) return false;

    $result = $this->db->preparedStatement($sql, $params);
    if (!$result) return false;

    // overwrite data with data from the database
    $logged_in_user_model->setId($result["id"]);
    $logged_in_user_model->setUserId($result["user_id"]);
    $logged_in_user_model->setSessionId($result["session_id"]);
    $logged_in_user_model->setLoggedInAt($result["logged_in_at"]);
    $logged_in_user_model->setLoggedInFrom($result["logged_in_from"]);
    $logged_in_user_model->setUserAgent($result["user_agent"]);
    $logged_in_user_model->setRememberMeToken($result["remember_me_token"]);

    return $logged_in_user_model;
  }

  /**
   * Inserts an active user model into the database
   * @param LoggedInUser $logged_in_user_model : the user to be inserted
   * @return bool : true on success,
   *                false otherwise
   */
  public function insertLoggedInUser(LoggedInUser $logged_in_user_model) {
    return $this->db->insertDatabaseEntry("logged_in_user", $logged_in_user_model);
  } // end insertLoggedInUser

  public function updateLoggedInUser(LoggedInUser $logged_in_user_model) {
    return $this->db->updateDatabaseEntry("logged_in_user", $logged_in_user_model->getId(), $logged_in_user_model);
  }

  /**
   * Deletes an active user from the database
   * @param $logged_in_id : id of the user to be deleted
   * @return bool : true on success,
   *                false otherwise
   */
  public function deleteLoggedInUser($logged_in_id) {
    return $this->db->deleteDatabaseEntry("logged_in_user", $logged_in_id);
  } // end deleteLoggedInUser

} // end MySqlLoggedInUserDAO