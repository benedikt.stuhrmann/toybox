<?php
require_once(APPLICATION_ROOT . "data" . DS . "ModuleDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "Module.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlUrlDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "Url.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlUserDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "User.php");

class MySqlModuleDAO implements ModuleDAO {

  private $db;
  private $logger;
  private $url_DAO;
  private $user_DAO;

  function __construct() {
    $this->db = new MySqlHelper();
    $this->db->openConnection();
    $this->logger = new Logger();
    $this->url_DAO = new MySqlUrlDAO();
    $this->user_DAO = new MySqluserDAO();
  }

  public function getModule(Module $module_model) {
    if (!$module_model) return false;

    $sql = "SELECT * FROM module WHERE";
    $params = array();

    $iteration = 1;
    foreach ($module_model->getProperties() as $property => $value) {
      if ($value === null) continue;
      if ($iteration != 1) $sql .= " AND";

      $sql .= " " . $property . " = ?";
      $params[] = $value;

      $iteration ++;
    }

    // if no values were set, return false
    if ($iteration == 1) return false;

    $result = $this->db->preparedStatement($sql, $params);
    if (!$result) return false;

    // overwrite data with data from the database
    $module_model->setId($result["id"]);
    $module_model->setName($result["name"]);
    $module_model->setCode($result["code"]);
    $module_model->setDescription($result["description"]);
    $module_model->setShortDescription($result["short_description"]);
    $module_model->setVisibility($result["visibility"]);

    $user_model = new User();
    $user_model->setId($result["created_by"]);
    $user_model = $this->user_DAO->getUser($user_model);
    if (!$user_model) return false;
    $module_model->setCreatedBy($user_model);

    $module_model->setCreatedAt($result["created_at"]);
    $module_model->setLastUpdated($result["last_updated"]);

    return $module_model;
  }

  /**
   * Gets all models from the database
   * @return array|bool : An array of module models,
   *                      false, if something failed
   */
  public function getModules() {
    $modules = $this->db->query("SELECT * FROM module");
    if (!$modules) return false;

    $module_models = array();
    foreach ($modules as $module) {
      // populate user model
      $module_model = new Module();
      $module_model->setId($module["id"]);
      $module_model->setName($module["name"]);
      $module_model->setCode($module["code"]);
      $module_model->setDescription($module["description"]);
      $module_model->setShortDescription($module["short_description"]);
      $module_model->setVisibility((int) $module["visibility"]);
      $user_model = new User();
      $user_model->setId($module["created_by"]);
      $user_model = $this->user_DAO->getUser($user_model);
      if (!$user_model) return false;
      $module_model->setCreatedBy($user_model);
      $module_model->setCreatedAt($module["created_at"]);
      $module_model->setLastUpdated($module["last_updated"]);
      // get url
      $url_model = new URL();
      $url_model->setCode("module_" . $module_model->getCode());
      $url_model = $this->url_DAO->getURL($url_model);
      // add to result array
      $module_models[] = array ("model" => $module_model,
                                "url" => $url_model->getUrl());
    }

    return $module_models;
  }

  /**
   * Creates a new module in the database,
   * @param Module $module_model : The module model, that gets inserted into the database
   * @return boolean : true, if a new module was created in the database,
   *                   false, if something went wrong
   */
  public function insertModule(Module $module_model) {
    return $this->db->insertDatabaseEntry("module", $module_model);
  } // end insertModule

  /**
   * Updates a module in the database
   * @param Module $module_model : the database entry with the id of the model will be updated.
   *                               Data in the database will be overwritten by data from the model
   * @return bool|mixed
   */
  public function updateModule(Module $module_model) {
    return $this->db->updateDatabaseEntry("module", $module_model->getId(), $module_model);
  } // end updateModule

  public function deleteModule($module_id) {
    return $this->db->deleteDatabaseEntry("module", $module_id);
  }

} // end MySqlModuleDAO