<?php
require_once(APPLICATION_ROOT . "data" . DS . "PageDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "Page.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlTemplateDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "Template.php");
require_once(APPLICATION_ROOT . "data" . DS . "mysql" . DS . "MySqlUrlDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "Url.php");

class MySqlPageDAO implements PageDAO {

  private $db;
  private $logger;
  private $template_DAO;
  private $url_DAO;

  function __construct() {
    $this->db = new MySqlHelper();
    $this->db->openConnection();
    $this->logger = new Logger();
    $this->template_DAO = new MySqlTemplateDAO();
    $this->url_DAO = new MySqlUrlDAO();
  }

  public function getPage(Page $page_model) {
    if (!$page_model) return false;

    $select = "SELECT *";
    $from = " FROM page";
    $on = "";
    $where = " WHERE";
    $params = array();

    $iteration = 1;
    foreach ($page_model->getProperties() as $property => $value) {
      if ($value === null) continue;
      if ($iteration != 1) $where .= " AND";

      if (is_object($value)) {
        $from .= " JOIN " . $property;
        if ($on == "") $on = " ON";
        $on .= " page." . $property . "_id = " . $property . ".id";

        foreach ($value->getProperties() as $prop => $val) {
          if ($val === null) continue;
          if ($iteration != 1) $where .= " AND";

          $where .= " " . $prop . " = ?";
          $params[] = $val;

          $iteration ++;

        }
      } else {
        $where .= " " . $property . " = ?";
        $params[] = $value;
        $iteration ++;
      }
    }

    // if no values were set, return false
    if ($iteration == 1) return false;

    $sql = $select . $from . $on . $where;
    $result = $this->db->preparedStatement($sql, $params);
    if (!$result) return false;

    // overwrite data with data from the database
    $page_model->setId($result["id"]);
    $page_model->setName($result["name"]);
    $page_model->setTitle($result["title"]);

    // get the template via its id
    $template_model = new Template();
    $template_model->setId($result["template_id"]);
    $template_model = $this->template_DAO->getTemplate($template_model);
    if (!$template_model) return false;
    $page_model->setTemplate($template_model);

    // get the url via its id
    $url_model = new Url();
    $url_model->setId($result["url_id"]);
    $url_model = $this->url_DAO->getUrl($url_model);
    if (!$url_model) return false;
    $page_model->setUrl($url_model);

    return $page_model;
  } // end getPage

  public function insertPage(Page $page_model) {
    $template_model = $page_model->getTemplate();
    if ($template_model) {
      if (!$this->template_DAO->getTemplate($template_model)) {  // insert if template does not exist already
        if (!$this->template_DAO->insertTemplate($template_model)) return false;
      }
      // get up to date data from database (otherwise might not have id)
      $template_model = $this->template_DAO->getTemplate($template_model);
      $page_model->setTemplate($template_model);
    } else {
      $this->logger->log("No Template model specified for insertion of new page");
      return false;
    }

    $url_model = $page_model->getUrl();
    if ($url_model) {
      if (!$this->url_DAO->getUrl($url_model)) { // insert if url does not exist already
        if (!$this->url_DAO->insertUrl($url_model)) return false;
      }
      // get up to date data from database (otherwise might not have id)
      $url_model = $this->url_DAO->getUrl($url_model);
      $page_model->setUrl($url_model);
    } else {
      $this->logger->log("No URL model specified for insertion of new page");
      return false;
    }

    $result = $this->db->insertDatabaseEntry("page", $page_model);
    if ($result !== true) {
      $this->logger->log("Could not insert Page into database: " . $result);
      return false;
    }
    return true;
  }

  public function updatePage(Page $page_model) {
    return $this->db->updateDatabaseEntry("page", $page_model->getId(), $page_model);
  }

  public function deletePage($page_id) {
    return $this->db->deleteDatabaseEntry("page", $page_id);
  }

} // end MySqlPageDAO