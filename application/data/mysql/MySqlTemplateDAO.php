<?php
require_once(APPLICATION_ROOT . "data" . DS . "TemplateDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "Template.php");

class MySqlTemplateDAO implements TemplateDAO {

  private $db;
  private $logger;

  function __construct() {
    $this->db = new MySqlHelper();
    $this->db->openConnection();
    $this->logger = new Logger();
  }

  public function getTemplate(Template $template_model) {
    if (!$template_model) return false;

    $template_data = $template_model->getProperties();
    $sql = "SELECT * FROM template WHERE";
    $params = array();

    $iteration = 1;
    foreach ($template_data as $property => $value) {
      if ($value === null) continue;
      if ($iteration != 1) $sql .= " AND";

      $sql .= " " . $property . " = ?";
      $params[] = $value;

      $iteration ++;
    }

    // if no values were set, return false
    if ($iteration == 1) return false;

    $result = $this->db->preparedStatement($sql, $params);
    if (!$result) return false;

    // overwrite data with data from the database
    $template_model->setId($result["id"]);
    $template_model->setCode($result["code"]);

    return $template_model;
  }

  public function insertTemplate(Template $template_model) {
    return $this->db->insertDatabaseEntry("template", $template_model);
  }

  public function updateTemplate(Template $template_model) {
    return $this->db->updateDatabaseEntry("template", $template_model->getId(), $template_model);
  }

  public function deleteTemplate($template_id) {
    return $this->db->deleteDatabaseEntry("template", $template_id);
  }

} // end MySqlTemplateDAO