<?php
require_once(APPLICATION_ROOT . "data" . DS . "UrlDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "Url.php");

class MySqlUrlDAO implements UrlDAO {

  private $db;
  private $logger;

  function __construct() {
    $this->db = new MySqlHelper();
    $this->db->openConnection();
    $this->logger = new Logger();
  }

  public function getUrl(Url $url_model) {
    if (!$url_model) return false;

    $url_data = $url_model->getProperties();
    $sql = "SELECT * FROM url WHERE";
    $params = array();

    $iteration = 1;
    foreach ($url_data as $property => $value) {
      if ($value === null) continue;
      if ($iteration != 1) $sql .= " AND";

      $sql .= " " . $property . " = ?";
      $params[] = $value;

      $iteration ++;
    }

    // if no values were set, return false
    if ($iteration == 1) return false;

    $result = $this->db->preparedStatement($sql, $params);
    if (!$result) return false;

    // overwrite data with data from the database
    $url_model->setId($result["id"]);
    $url_model->setCode($result["code"]);
    $url_model->setUrl($result["url"]);

    return $url_model;
  }

  /**
   * Inserts an URL model into the database
   * @param Url $url_model : the url model to be inserted into the database
   * @return bool : true on success,
   *                false on failure
   */
  public function insertUrl(Url $url_model) {
    $result = $this->db->insertDatabaseEntry("url", $url_model);
    if ($result !== true) {
      $this->logger->log("Could not insert URL into database: " . $result);
      return false;
    }
    return true;
  }

  public function updateUrl(Url $url_model) {
    return $this->db->updateDatabaseEntry("url", $url_model->getId(), $url_model);
  }

  public function deleteUrl($url_id) {
    return $this->db->deleteDatabaseEntry("url", $url_id);
  }

} // end MySqlUrlDAO