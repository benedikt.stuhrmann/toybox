<?php
require_once(APPLICATION_ROOT . "data" . DS . "UserDAO.php");
require_once(APPLICATION_ROOT . "models" . DS . "User.php");

class MySqlUserDAO implements UserDAO {

  private $db;
  private $logger;

  function __construct() {
    $this->db = new MySqlHelper();
    $this->db->openConnection();
    $this->logger = new Logger();
  }

  public function getUser(User $user_model) {
    if (!$user_model) return false;

    $sql = "SELECT * FROM user WHERE";
    $params = array();

    $iteration = 1;
    foreach ($user_model->getProperties() as $property => $value) {
      if ($value === null) continue;
      if ($iteration != 1) $sql .= " AND";

      $sql .= " " . $property . " = ?";
      $params[] = $value;

      $iteration ++;
    }

    // if no values were set, return false
    if ($iteration == 1) return false;

    $result = $this->db->preparedStatement($sql, $params);
    if (!$result) return false;

    // overwrite data with data from the database
    $user_model->setId($result["id"]);
    $user_model->setUsername($result["username"]);
    $user_model->setFirstName($result["first_name"]);
    $user_model->setLastName($result["last_name"]);
    $user_model->setEmail($result["email"]);
    $user_model->setPassword($result["password"]);
    $user_model->setLastSeen($result["last_seen"]);
    $user_model->setLastUpdated($result["last_updated"]);
    $user_model->setCreatedAt($result["created_at"]);
    $user_model->setPasswordResetToken($result["password_reset_token"]);

    return $user_model;
  } // end getUser

  /**
   * Gets all users from the database
   * @return array|bool : An array of user models,
   *                      false, if something failed
   */
  public function getUsers() {
    $users = $this->db->query("SELECT * FROM user");
    if (!$users) return false;

    $user_models = array();
    foreach ($users as $user) {
      // populate user model
      $user_model = new User();
      $user_model->setId($user["id"]);
      $user_model->setUsername($user["username"]);
      $user_model->setFirstName($user["first_name"]);
      $user_model->setLastName($user["last_name"]);
      $user_model->setEmail($user["email"]);
      $user_model->setPassword($user["password"]);
      $user_model->setLastSeen($user["last_seen"]);
      $user_model->setLastUpdated($user["last_updated"]);
      $user_model->setCreatedAt($user["created_at"]);
      $user_model->setPasswordResetToken($user["password_reset_token"]);
      // add to result array
      $user_models[] = $user_model;
    }

    return $user_models;
  } // getUsers

  /**
   * Creates a new user in the database,
   * @param User $user_model : The user model, that gets inserted into the database
   * @return boolean : true, if a new user was created in the database,
   *                   false, if something went wrong
   */
  public function insertUser(User $user_model) {
    return $this->db->insertDatabaseEntry("user", $user_model);
  } // end insertUser

  public function updateUser(User $user_model) {
    return $this->db->updateDatabaseEntry("user", $user_model->getId(), $user_model);
  } // end updateUser

  public function deleteUser($user_id) {
    return $this->db->deleteDatabaseEntry("user", $user_id);
  }

} // end MySqlUserDAO