<?php
require_once("Model.php");

class LoggedInUser extends Model {

  protected $user_id;
  protected $session_id;
  protected $logged_in_at;
  protected $logged_in_from;
  protected $user_agent;
  protected $remember_me_token;

  /**
   * @param int $user_id
   */
  public function setUserId($user_id) {
    $this->user_id = $user_id;
  }

  /**
   * @return int $user_id
   */
  public function getUserId() {
    return $this->user_id;
  }

  /**
   * @param string $session_id
   */
  public function setSessionId($session_id) {
    $this->session_id = $session_id;
  }

  /**
   * @return string $session_id
   */
  public function getSessionId() {
    return $this->session_id;
  }

  /**
   * @param string $logged_in_at
   */
  public function setLoggedInAt($logged_in_at) {
    $this->logged_in_at = $logged_in_at;
  }

  /**
   * @return string $logged_in_at
   */
  public function getLoggedInAt() {
    return $this->logged_in_at;
  }

  /**
   * @param string $logged_in_from
   */
  public function setLoggedInFrom($logged_in_from) {
    $this->logged_in_from = $logged_in_from;
  }

  /**
   * @return string $logged_in_from
   */
  public function getLoggedInFrom() {
    return $this->logged_in_from;
  }

  /**
   * @param string $user_agent
   */
  public function setUserAgent($user_agent) {
    $this->user_agent = $user_agent;
  }

  /**
   * @return string $user_agent
   */
  public function getUserAgent() {
    return $this->user_agent;
  }

  /**
   * @param string $remember_me_token
   */
  public function setRememberMeToken($remember_me_token) {
    $this->remember_me_token = $remember_me_token;
  }

  /**
   * @return string $remember_me_token
   */
  public function getRememberMeToken() {
    return $this->remember_me_token;
  }

} // end LoggedInUser