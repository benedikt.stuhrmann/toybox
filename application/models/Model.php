<?php

class Model {

  protected $id;

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int $id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return array : An array with "property" => "value" associations for all properties
   */
  public function getProperties() {
    $properties = array();
    foreach ($this as $property => $value) {
      $properties[$property] = $value;
    }
    return $properties;
  }

} // end Model
