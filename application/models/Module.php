<?php
require_once("Model.php");

class Module extends Model {

  protected $name;
  protected $code;
  protected $description;
  protected $short_description;
  protected $visibility;
  protected $created_by;
  protected $created_at;
  protected $last_updated;

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string $name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $code
   */
  public function setCode($code) {
    $this->code = $code;
  }

  /**
   * @return string $code
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return string $description
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $short_description
   */
  public function setShortDescription($short_description) {
    $this->short_description = $short_description;
  }

  /**
   * @return string $short_description
   */
  public function getShortDescription() {
    return $this->short_description;
  }

  /**
   * @param int $visibility
   */
  public function setVisibility($visibility) {
    $this->visibility = $visibility;
  }

  /**
   * @return int $visibility
   */
  public function getVisibility() {
    return $this->visibility;
  }

  /**
   * @param string $created_by
   */
  public function setCreatedBy($created_by) {
    $this->created_by = $created_by;
  }

  /**
   * @return string $created_by
   */
  public function getCreatedBy() {
    return $this->created_by;
  }
  
  /**
   * @param string $created_at
   */
  public function setCreatedAt($created_at) {
    $this->created_at = $created_at;
  }

  /**
   * @return string $created_at
   */
  public function getCreatedAt() {
    return $this->created_at;
  }

  /**
   * @param string $last_updated
   */
  public function setLastUpdated($last_updated) {
    $this->last_updated = $last_updated;
  }

  /**
   * @return string $last_updated
   */
  public function getLastUpdated() {
    return $this->last_updated;
  }



} // end Module
