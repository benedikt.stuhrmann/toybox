<?php
require_once("Model.php");

class Page extends Model {

  protected $name;
  protected $template;
  protected $url;
  protected $title;

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string $name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param Template $template
   */
  public function setTemplate(Template $template) {
    $this->template = $template;
  }

  /**
   * @return Template $template
   */
  public function getTemplate() {
    return $this->template;
  }

  /**
   * @param Url $url
   */
  public function setUrl(Url $url) {
    $this->url = $url;
  }

  /**
   * @return Url $url
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @param string $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * @return string $title
   */
  public function getTitle() {
    return $this->title;
  }

} // end Page
