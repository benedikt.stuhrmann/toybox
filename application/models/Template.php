<?php
require_once("Model.php");

class Template extends Model{

  protected $code;

  /**
   * @param string $code
   */
  public function setCode($code) {
    $this->code = $code;
  }

  /**
   * @return string $code
   */
  public function getCode() {
    return $this->code;
  }

} // end Template
