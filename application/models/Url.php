<?php
require_once("Model.php");

class Url extends Model {

  protected $code;
  protected $url;

  /**
   * @param string $code
   */
  public function setCode($code) {
    $this->code = $code;
  }

  /**
   * @return string $code
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @param string $url
   */
  public function setUrl($url) {
    if ($url) $this->url = rtrim($url, "/") . "/";
  }

  /**
   * @return string $url
   */
  public function getUrl() {
    return $this->url;
  }

} // end Url
