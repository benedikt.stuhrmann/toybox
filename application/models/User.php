<?php
require_once("Model.php");

class User extends Model {

  protected $username;
  protected $first_name;
  protected $last_name;
  protected $email;
  protected $password;
  protected $last_seen;
  protected $last_updated;
  protected $created_at;
  protected $password_reset_token;

  /**
   * @param string $username
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * @return string username
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * @param string $first_name
   */
  public function setFirstName($first_name) {
    $this->first_name = $first_name;
  }

  /**
   * @return string first_name
   */
  public function getFirstName() {
    return $this->first_name;
  }

  /**
   * @param string $last_name
   */
  public function setLastName($last_name) {
    $this->last_name = $last_name;
  }

  /**
   * @return string last_name
   */
  public function getLastName() {
    return $this->last_name;
  }

  /**
   * @param string $email
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * @return string email
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param string $password
   */
  public function setPassword($password) {
    $this->password = $password;
  }

  /**
   * @return string password
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * @param string $last_seen
   */
  public function setLastSeen($last_seen) {
    $this->last_seen = $last_seen;
  }

  /**
   * @return string $last_seen
   */
  public function getLastSeen() {
    return $this->last_seen;
  }
  
  /**
   * @param string $last_updated
   */
  public function setLastUpdated($last_updated) {
    $this->last_updated = $last_updated;
  }

  /**
   * @return string $last_updated
   */
  public function getLastUpdated() {
    return $this->last_updated;
  }

  /**
   * @param string $created_at
   */
  public function setCreatedAt($created_at) {
    $this->created_at = $created_at;
  }

  /**
   * @return string $created_at
   */
  public function getCreatedAt() {
    return $this->created_at;
  }

  /**
   * @param string $password_reset_token
   */
  public function setPasswordResetToken($password_reset_token) {
    $this->password_reset_token = $password_reset_token;
  }

  /**
   * @return string $password_reset_token
   */
  public function getPasswordResetToken() {
    return $this->password_reset_token;
  }
  
} // end User