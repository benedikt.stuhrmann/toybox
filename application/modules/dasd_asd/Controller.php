<?php

class Controller {

  /**
   * Gets called automagically whenever the module is accessed.
   * @param $template : Your smarty template object. Can be used to assign variables to the template
   */
  public function handle($template) {

  } // end handle

} // end Controller
