class Matrix {

  /**
   * Creates a new matrix with the specified amount of rows and columns.
   * All values are 0 initially.
   */
  constructor(rows, cols) {
    this.rows = rows;
    this.cols = cols;

    // initialize matrix with 0
    this.values = [...Array(this.rows)].map(e => Array(this.cols).fill(0));
  } // end constructor

  /**
   * Logs the matrix on the console.
   */
  log() {
    console.table(this.values);
  } // end log

  /**
   * Converts a given array into a matrix.
   */
  static fromArray(array) {
    let matrix = new Matrix(array.length, 1);
    for (let i=0; i<array.length; i++)
      matrix.values[i][0] = array[i];
    return matrix;
  } // end fromArray

  /**
   * Converts the matrix into an array.
   */
  toArray() {
    let array = [];
    for (let i=0; i<this.rows; i++) {
      for (let j=0; j<this.cols; j++) {
        array.push(this.values[i][j])
      }
    }
    return array;
  } // end toArray

  /**
   * Transposes the given matrix and returns it.
   */
  static transpose(matrix) {
    let matrix_t = new Matrix(matrix.cols, matrix.rows);
    for (let i=0; i<this.rows; i++) {
      for (let j=0; j<this.cols; j++) {
        matrix_t.values[j][i] = matrix.values[i][j];
      }
    }
    return matrix_t;
  } // end transpose

  /**
   * Multiplies the matrix with n.
   * If n is a matrix, then the ressult is the hadamard product.
   * If n is a number, then the reuslt is the scalar product.
   */
  multiply(n) {
    if (n instanceof Matrix) {
      // hadamard product
      for (let i=0; i<this.rows; i++) {
        for (let j=0; j<this.cols; j++) {
          this.values[i][j] *= n.values[i][j];
        }
      }
    } else {
      // scalar product
      for (let i=0; i<this.rows; i++) {
        for (let j=0; j<this.cols; j++) {
          this.values[i][j] *= n;
        }
      }
    }
  } // end multiply

  /**
   * Returns the matrix product of matrix_a and matrix_b.
   * Make sure the columns of matrix_a match the rows of matrix_b.
   */
  static multiply(matrix_a, matrix_b) {
    if (matrix_a.cols !== matrix_b.rows) {
      console.log("Columns of matrix_a must match the rows of matrix_b.");
      return;
    }

    let product = new Matrix(matrix_a.rows, matrix_b.cols);
    for (let i=0; i<product.rows; i++) {
      for (let j=0; j<product.cols; j++) {
        let sum = 0;
        for (let k=0; k<matrix_a.cols; k++) {
          sum += matrix_a.values[i][k] * matrix_b.values[k][j];
        }
        product.values[i][j] = sum;
      }
    }
    return product;
  } // end multiply

  /**
   * Applies the given function to each element in the matrix.
   */
  map(func) {
    for (let i=0; i<this.rows; i++) {
      for (let j=0; j<this.cols; j++) {
        this.values[i][j] = func(this.values[i][j]);
      }
    }
  } // end map

  /**
   * Applies the given function to each element in the given matrix.
   */
  static map(matrix, func) {
    for (let i=0; i<matrix.rows; i++) {
      for (let j=0; j<matrix.cols; j++) {
        matrix.values[i][j] = func(matrix.values[i][j]);
      }
    }
    return matrix;
  } // end map

  add(n) {
    if (n instanceof Matrix) {
      if (n.cols !== this.cols) {
        console.log("Columns of n must match the columns of the matrix.");
        return;
      }
      if (n.rows !== this.rows) {
        console.log("Rows of n must match the rows of the matrix.");
        return;
      }

      // add each component to its counterpart
      for (let i=0; i<this.rows; i++) {
        for (let j=0; j<this.cols; j++) {
          this.values[i][j] += n.values[i][j];
        }
      }
    } else {
      // add n to every value
      for (let i=0; i<this.rows; i++) {
        for (let j=0; j<this.cols; j++) {
          this.values[i][j] += n;
        }
      }
    }
  } // end add

  /**
   * Sets each matrix element to a random value.
   */
  randomize(min, max) {
    for (let i=0; i<this.rows; i++) {
      for (let j=0; j<this.cols; j++) {
        this.values[i][j] = Math.random();
      }
    }
  } // end randomize

} // end Matrix
