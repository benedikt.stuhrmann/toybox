class NeuralNetwork {

  /**
   * Creates a new neural network with
   * one input layer with inputs input-neurons,
   * hidden.length hidden layers with hidden[i] hidden-neurons
   * and one output layer with outputs output-neurons
   */
  constructor(inputs, hidden, outputs) {
    // set up layers
    // hidden layers + 2 (input layer and output layer)
    (this.layers = []).length = (hidden.length + 2);
    for (let i=0; i<this.layers.length; i++) {
      let layer = {
        weights : null,
        neurons : null,
        biases  : null
      }

      if (i == 0) {
        // first layer = input layer (has no weights and no biases)
        layer.neurons = new Matrix(inputs, 1);//Array.apply(null, Array(inputs)).map(Number.prototype.valueOf,0);
      } else if (i == this.layers.length-1) {
        // last layer = output layer
        layer.weights = new Matrix(outputs, this.layers[i-1].neurons.rows);
        layer.neurons = new Matrix(outputs, 1)//Array.apply(null, Array(outputs)).map(Number.prototype.valueOf,0);
        layer.biases  = new Matrix(layer.neurons.rows, 1);
      } else {
        // some hidden layer
        layer.weights = new Matrix(hidden[i-1], this.layers[i-1].neurons.rows);
        layer.neurons = new Matrix(hidden[i-1], 1)//Array.apply(null, Array(hidden[i-1])).map(Number.prototype.valueOf,0);
        layer.biases  = new Matrix(layer.neurons.rows, 1);
      }

      if (layer.weights && layer.biases) {
        layer.weights.randomize(0, 1);
        layer.biases.randomize(0, 1);
      }

      this.layers[i] = layer;
    }

    /*
    for (let i=0; i<this.layers.length; i++) {
      if (i == 0) console.log("Layer " + (i+1) + " (input layer):");
      else if (i == this.layers.length-1) console.log("Layer " + (i+1) + " (output layer):");
      else console.log("Layer " + (i+1) + " (hidden layer):");
      console.log("Neurons: " + this.layers[i].neurons.rows);
      this.layers[i].neurons.log();
      if (!this.layers[i].weights) console.log("No Weights");
      else {
        console.log("Weights: ");
        this.layers[i].weights.log();
      }
      if (!this.layers[i].biases) console.log("No Biases");
      else {
        console.log("Biases: ");
        this.layers[i].biases.log();
      }
      console.log("------------------------");
    }*/

  } // end constructor

  /**
   * Feeds the input_values into the neural network and returns the generated output.
   */
  feedForward(input_values) {
    // check if there's the correct amount of input values for the network
    if (input_values.length !== this.layers[0].neurons.rows) {
      console.log("Amount of input values (" + input_values.length + ") does not match the amount of input neurons (" + this.layers[0].neurons.rows + ")!")
      return;
    }

    // set the input values
    this.layers[0].neurons = Matrix.fromArray(input_values);

    // calculate the new neuron outputs for each layer in the network
    for (let i=1; i<this.layers.length; i++) {
        // multiply input into the neurons (output of neurons from previous layer) with its weights
        this.layers[i].neurons = Matrix.multiply(this.layers[i].weights, this.layers[i-1].neurons);
        // add the biases
        this.layers[i].neurons.add(this.layers[i].biases);
        // apply activation function
        this.layers[i].neurons.map(this.sigmoid);
    }

    // return the output (last layer)
    return this.layers[this.layers.length-1].neurons.toArray();
  } // end feedForward

  sigmoid(x) {
    return 1 / (1 + Math.exp(-x));
  } // end sigmoid

} // end NeuralNetwork
