$(document).ready(function() {

  let options = {
      "selector"  : "#my-snake-game"
    , "grid"      : 21
    , "size"      : 800
    , "fps"       : 12
    , "length"    : 5
    , "snakeview" : false
  }
  const game = new SnakeGame(options);

  $("a#toggle-snakeview").on("click", function() {
    $(this).toggleClass("active");
    game.toggleSnakeview();
  });

  let neural_network;
  let ouput;
  let input = [0.2, 1.7];
  for (var i=0; i<100; i++) {
    neural_network = new NeuralNetwork(2, [3, 2], 1);
    output = neural_network.feedForward(input);
    console.log("Input: " + input);
    console.log("Output: " + output);
  }

});
