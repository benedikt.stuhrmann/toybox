class SnakeGame {

  /**
   * Creates a new SnakeGame instance.
   * options : {
   *   "selector"  : jQuery selector of the parent for the canvas
   *   "grid"      : grid size in units
   *   "size"      : canvas size in px
   *   "fps"       : frames per seconds
   *   "length"    : length of the snake when game starts
   *   "snakeview" : shows an overlay with the all the data the neuro snake gets
   * }
   */
  constructor(options) {
    if(!options.selector) {
      Debug.Log("ERROR: Can't start snake game. No selector speciefied.")
      return;
    }

    this.initializeGame = this.initializeGame.bind(this);
    this.resetGame = this.resetGame.bind(this);
    this.update = this.update.bind(this);
    this.draw = this.draw.bind(this);
    this.updateSnake = this.updateSnake.bind(this);
    this.spawnFood = this.spawnFood.bind(this);
    this.input = this.input.bind(this);
    this.updateHighscore = this.updateHighscore.bind(this);

    // default options
    let default_options = {
        "selector" : null
      , "grid"     : 15
      , "size"     : 600
      , "fps"      : 15
      , "length"   : 5
      , "snakeview" : false
    };

    // overwrite default options with custom options
    if(options) this.options = {...default_options, ...options};
    else this.options = default_options;

    this.options.grid += 2; // walls

    // generate id for canvas
    this.id = Math.floor(Math.random() * 1000);

    // create html struture with the canvas
    // add basic structure
    $(this.options.selector).append("<div id='snake-game-" + this.id + "' class='snake-game-box'><div class='info-top'></div></div>");
    // add score and highscore
    $("#snake-game-" + this.id + " .info-top").append("<span class='score'>Score: <span>0</span></span><span class='highscore'>Highscore: <span>0</span></span>");
    // add canvas
    $("#snake-game-" + this.id).append("<canvas id='snake-game-" + this.id + "' class='snake-game' width='" + this.options.size + "' height='" + this.options.size + "'></canvas>");
    this.canvas = $("#snake-game-" + this.id + " canvas");
    this.context = this.canvas[0].getContext("2d");

    // set game speed/fps
    setInterval(this.update, 1000/this.options.fps);

    // register keyevents
    $(document).on("keydown", this.input);

    this.initializeGame();
  } // end constructor

  /**
   * Initializes the game setup
   */
  initializeGame() {
    // initialize the grid
    this.grid = [...Array(this.options.grid)].map(e => Array(this.options.grid).fill(0));
    for (let y=0; y<this.grid.length; y++) {
      for (let x=0; x<this.grid[y].length; x++) {
        // put walls around the grid
        if (x == 0 || y == 0 || x == this.grid.length-1 || y == this.grid.length-1) this.grid[y][x] = 9999;
      }
    }

    // create the starter snake
    this.snake = new Array(this.options.length);
    // head of the snake starts in the middle of the grid
    this.snake[0] = {
      x : Math.floor(this.options.grid/2),
      y : Math.floor(this.options.grid/2)
    }
    this.grid[this.snake[0].y][this.snake[0].x] = this.snake.length;
    for(let i=1; i<this.snake.length; i++) {
      // set x and y of other snake elements
      this.snake[i] = {
        x : this.snake[i-1].x,
        y : this.snake[i-1].y + 1
      }

      // also put them into the grid initially
      this.grid[this.snake[i].y][this.snake[i].x] = this.snake.length-i;
    }

    // initial snake velocity
    this.velocity = {
      x : 0,
      y : 0
    }

    this.game_over = false;
    this.score = 0;
  } // end initializeGame

  /**
   * Ends the game and then initializes a new one
   */
  resetGame() {
    this.food = null;
    $(this.options.selector + " .score span").html(0);
    this.updateHighscore(this.score);
    this.initializeGame();
  } // end resetGame

  /**
   * Update method called every single frame
   */
  update() {
    if (this.game_over) return;
    if (!this.food) this.spawnFood();

    // if the snake is moving, update it!
    if (this.velocity.x != 0 || this.velocity.y != 0)
      this.updateSnake();

    this.draw();
  } // end update

  /**
   * Outputs the current game status on the canvas
   */
  draw() {
    let ctx = this.context;
    let unit_size = this.options.size/this.options.grid;

    // clear canvas
    ctx.clearRect(0, 0, this.options.size, this.options.size);

    // draw the grid, with the snake and food in it
    for (let y=0; y<this.grid.length; y++) {
      for (let x=0; x<this.grid[y].length; x++) {
        if (this.grid[y][x] == 9999) {
          // wall tile
          ctx.fillStyle = "lightgray";
        }
        else if (this.grid[y][x] > 0) {
          // snake tile
          let a = this.grid[y][x];
          a = (this.normalize(a, 0, this.snake.length));
          ctx.fillStyle = "rgba(0, 120, 255, " + (a+0.3) + ")";
        }
        else if (this.grid[y][x] == -1) {
          // food tile
          ctx.fillStyle = "#FF9A00";
        }
        else if (this.grid[y][x] == 0) {
          // background tile
          ctx.fillStyle = "honeydew";
        }

        ctx.fillRect(x*unit_size, y*unit_size, unit_size, unit_size);

        if (this.options.snakeview) {
          ctx.font = "12px Verdana";
          ctx.textAlign = "center";
          ctx.fillStyle = "black";
          //ctx.fillText(x + ", " + y, (x*unit_size)+(unit_size/2), (y*unit_size)+(unit_size/2));
          ctx.fillText(this.grid[y][x], (x*unit_size)+(unit_size/2), (y*unit_size)+(unit_size/2)+5);
        }

      } // end cols
    } // end rows

  } // end draw

  /**
   * Updates the snake and the grid according to the input/velocity
   */
  updateSnake() {
    // new position of the snake
    let new_x_pos = this.snake[0].x + this.velocity.x;
    let new_y_pos = this.snake[0].y + this.velocity.y;

    // indicates if the snake found food or not
    let food = false;

    if (this.grid[new_y_pos][new_x_pos] > 0) {
      // snake hit the wall or itself
      this.game_over = true;
      return;
    } else if (this.grid[new_y_pos][new_x_pos] == -1) {
      // snake hit food
      food = true;
    }

    let last = null;
    for(let i=this.snake.length-1; i>0; i--) {
      // save position of last snake element
      if (i == this.snake.length-1) {
        last = {
          x : this.snake[i].x,
          y : this.snake[i].y
        };
      }

      // update positions of all snake elements except the head
      // new position = position of previous element
      this.snake[i] = {
        x : this.snake[i-1].x,
        y : this.snake[i-1].y
      }

      // also update the grid
      this.grid[this.snake[i].y][this.snake[i].x] = this.snake.length-i;
      if (last) this.grid[last.y][last.x] = 0;
    } // end for

    // set head to new position
    this.snake[0] = {
      x : new_x_pos,
      y : new_y_pos
    };
    this.grid[this.snake[0].y][this.snake[0].x] = this.snake.length;

    if (food) {
      // snake hit food
      this.snake.push(last); // make it longer!
      this.score ++;
      this.food = null;
      $(this.options.selector + " .score span").html(this.score);
    }
  } // end updateSnake

  /**
   * Spawns food at a random free grid tile
   */
  spawnFood() {
    this.food = {
      x : null,
      y : null
    };

    // randomly spawn food on a free tile
    do {
      this.food.x = Math.floor(Math.random() * this.grid.length);
      this.food.y = Math.floor(Math.random() * this.grid.length);
    } while(this.grid[this.food.y][this.food.x] != 0);

    // set food in grid
    this.grid[this.food.y][this.food.x] = -1;
  }

  /**
   * Normalizes a value in the interval [min, max] to a value between 0 and 1
   */
  normalize(value, min, max) {
    return (value - min) / (max - min);
  } // end normalize

  /**
   * Get input and change movement of snake in next frame accordingly
   */
  input(event) {
    if (!event) return;

    switch (event.keyCode) {
      case 38: // arrow up
      case 87: // w
        // go upwards (if not going downwards)
        if (!(this.velocity.x == 0 && this.velocity.y == 1)) {
          this.velocity.x = 0;
          this.velocity.y = -1;
        }
        break;
      case 39: // arrow right
      case 68: // d
        // go to the right (if not going to the left)
        if (!(this.velocity.x == -1 && this.velocity.y == 0)) {
          this.velocity.x = 1;
          this.velocity.y = 0;
        }
        break;
      case 40: // arrow down
      case 83: // s
        // go downwards (if not going upwards)
        if (!(this.velocity.x == 0 && this.velocity.y == -1)) {
          this.velocity.x = 0;
          this.velocity.y = 1;
        }
        break;
      case 37: // arrow left
      case 65: // a
        // go to the left (if not going to the right)
        if (!(this.velocity.x == 1 && this.velocity.y == 0)) {
          this.velocity.x = -1;
          this.velocity.y = 0;
        }
        break;
    }

    // if game over => reset
    if (this.game_over) this.resetGame();

  } // end input

  /**
   * Updates the highscore if the new_score is higher than the existing highscore
   */
  updateHighscore(new_score) {
    if (!this.highscore) this.highscore = 0;
    if (new_score > this.highscore) this.highscore = new_score;
    $("#snake-game-" + this.id + " .highscore span").html(this.highscore);
  } // end updateHighscore

  /**
   * Enables/Disabled the Snakeview
   */
  toggleSnakeview() {
    this.options.snakeview = !this.options.snakeview;
  } // end toggleSnakeView

} // end SnakeGame
