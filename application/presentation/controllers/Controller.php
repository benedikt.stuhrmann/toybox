<?php

abstract class Controller {

  /**
   * Gets called every time when a User accesses the belonging page
   * @param Smarty $template : The smarty template, that will be rendered after the controller finished
   */
  abstract public function handle($template);

} // end controller
