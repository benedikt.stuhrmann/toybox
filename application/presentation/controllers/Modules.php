<?php

require_once(APPLICATION_ROOT . "businesslogic" . DS . "ModuleManager.php");
require_once(APPLICATION_ROOT . "businesslogic" . DS . "UserManager.php");
require_once(APPLICATION_ROOT . "businesslogic" . DS . "Validator.php");

class ModulesController extends Controller {

  private $module_manager;
  private $user_manager;

  function __construct() {
    $this->module_manager = new ModuleManager();
    $this->user_manager = new UserManager();
  } // end constructor


  public function handle($template, $form_data = null) {
    $modules = $this->module_manager->getModules();
    $template->assign("modules", $modules);

    $user_model_session = $this->user_manager->getLoggedInUser($template);
    if ($form_data) {
      $result = $this->saveModule($form_data, $user_model_session);
      if ($result !== true) return "error";

      $module_model = new Module();
      $module_model->setCode($form_data["code"]);
      $module_model = $this->module_manager->getModule($module_model);
      $result = $this->saveModuleStructure($module_model, $form_data["thumbnail"]);
      if ($result !== true) return "error";
    }
  } // end handle

  /**
   * Saves the data from the form and updates the given module. If no module is passed, a new module will be created.
   * @param array $module_data : the form data, with the module information
   * @param user $user_model   : model of the logged in user
   * @param Module $old_module : the old module_model, that has to be updated
   *                             (default : null -> new module will be created)
   * @return boolean|array : true, if update/creation was successful,
   *                   an array of errors otherwise
   */
  private function saveModule($module_data, User $user_model, Module $old_module = null) {
    $new_module_model = new Module();

    // set id of existing user, if it's an update
    if ($old_module) $new_module_model->setId($old_module->getId());

    // validate the data from the form
    $validator = new Validator("de");
    $validator->add("name",              safe($module_data, "name", ""),              ["required" => true, "minlength" => 4, "maxlength" => 40]);
    $validator->add("url",               safe($module_data, "url", ""),               ["required" => true, "minlength" => 3, "maxlength" => 20]);
    $validator->add("code",              safe($module_data, "code", ""),              ["required" => true, "minlength" => 3, "maxlength" => 20, "modulecode" => true]);
    $validator->add("description",       safe($module_data, "description", ""),       ["minlength" => 10, "maxlength" => 5000]);
    $validator->add("short_description", safe($module_data, "short-description", ""), ["minlength" => 10, "maxlength" => 400]);

    if (!$validator->validate()) return $validator->getErrors();

    $new_module_model->setName($module_data["name"]);
    $new_module_model->setCode($module_data["code"]);
    $new_module_model->setDescription($module_data["description"]);
    $new_module_model->setShortDescription($module_data["short-description"]);
    $visibility = safe($module_data, "public", "off");
    $visibility = ($visibility === "on") ? 1 : 0;
    $new_module_model->setVisibility($visibility);
    $new_module_model->setCreatedBy($user_model->getId());
    $new_module_model->setCreatedAt(date("Y:m:d H:i:s"));
    $new_module_model->setLastUpdated(date("Y:m:d H:i:s"));

    // if module id exists, update existing module, otherwise create a new one
    $update_id = ($old_module) ? $old_module->getId() : false;
    return $this->module_manager->createUpdateModule($new_module_model, $module_data["url"], $update_id);
  } // end saveModule

  /**
   * Creates the folder and file structure for the new module.
   * Creates an index.html, Controller.php, styles.scss and the submitted thumbnail
   * @param Module $module_model : The database model of the submitted module data
   * @param string $base64_thumb : The uploaded thumbnail encoded in base64
   * @return bool : false, if something went wrong
   */
  private function saveModuleStructure(Module $module_model, $base64_thumb = "") {
    $binary_thumb = file_get_contents($base64_thumb);
    return $this->module_manager->createUpdateModuleStructure($module_model, $binary_thumb);
  } // end saveModuleStructure

} // end controller
