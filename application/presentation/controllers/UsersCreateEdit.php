<?php

require_once(APPLICATION_ROOT . "businesslogic" . DS . "UserManager.php");
require_once(APPLICATION_ROOT . "businesslogic" . DS . "Validator.php");

class UsersCreateEditController extends Controller {

  private $user_manager;

  function __construct() {
    $this->user_manager = new UserManager();
  } // end constructor

  public function handle($template, $form_data = null) {
    $user_model_session = $this->user_manager->getLoggedInUser($template);
    if (!$user_model_session) redirect("error_403", 403);

    if(strpos(safe($_GET, "url"), "account") !== false || safe($form_data, "save") == "edit") $action = "edit";
    else $action = "new";
    $template->assign("action", $action);

    $user_model = new User();
    if ($action === "edit") {
      // get user data from the database instead of from the session
      $user_model = new User();
      $user_model->setId($user_model_session->getId());
      $user_model = $this->user_manager->getUser($user_model);
    }
    $template->assign("user", $user_model);

    // when submitting the form
    if ($form_data) return $this->handleSubmit($form_data, $action, $user_model);
  } // end handle

  private function handleSubmit($form_data, $action, $user_model) {
    $response = array ( "answ" => "error" );

    if ($form_data && $action === "edit" && $user_model) $result = $this->saveUser($form_data, $user_model);
    else if ($form_data && $action === "new")            $result = $this->saveUser($form_data);

    if ($result) {
      if ($result === true) $response["answ"] = "success";
      else $response["answ"] = $result;
    }

    return $response;
  } // end handleSubmit

  /**
   * Saves the data from the from and updates the given user. If no user is passed, a new user will be created.
   * @param array $user_data : the form data, with the user information
   * @param User $old_user_model : the old user_model, that has to be updated
   *                               (default : null -> new user will be created)
   * @return true, if update/creation was successful,
   *         an array of errors otherwise
   */
  private function saveUser($user_data, User $old_user_model = null) {
    $new_user_model = new User();

    // set id of existing user, if it's an update
    if ($old_user_model) $new_user_model->setId($old_user_model->getId());

    // validate the data from the form
    $validator = new Validator("de");
    $validator->add("username",        safe($user_data, "username", ""),        ["required" => true, "minlength" => 4, "maxlength" => 50]);
    $validator->add("firstname",       safe($user_data, "first-name", ""),      ["required" => true, "minlength" => 2, "maxlength" => 20]);
    $validator->add("lastname",        safe($user_data, "last-name", ""),       ["required" => true, "minlength" => 2, "maxlength" => 20]);
    $validator->add("email",           safe($user_data, "email", ""),           ["required" => true, "email" => true]);
    $validator->add("email_repeat",    safe($user_data, "email-repeat", ""),    ["required" => true, "email" => true, "match" => $user_data["email"]]);
    if (!$user_data["password"] && $old_user_model) {
      // if its an update of an existing user, and no password is given. Keep the old password.
      $user_data["password"] = $old_user_model->getPassword();
    } else {
      // otherwise, validate the new password
      $validator->add("password",        safe($user_data, "password", ""),        ["required" => true, "password" => true, "maxlength" => 50]);
      $validator->add("password_repeat", safe($user_data, "password-repeat", ""), ["required" => true, "password" => true, "maxlength" => 50, "match" => $user_data["password"]]);
      $user_data["password"] = md5(safe($user_data, "password"));
    }

    if (!$validator->validate()) return $validator->getErrors();

    if (!$old_user_model || ($old_user_model && $user_data["username"] !== $old_user_model->getUsername()))
      $new_user_model->setUsername($user_data["username"]);
    $new_user_model->setFirstName($user_data["first-name"]);
    $new_user_model->setLastName($user_data["last-name"]);
    if (!$old_user_model || ($old_user_model && $user_data["email"] !== $old_user_model->getEmail()))
      $new_user_model->setEmail($user_data["email"]);
    $new_user_model->setPassword($user_data["password"]);
    if (!$old_user_model)
      $new_user_model->setCreatedAt(date("Y:m:d H:i:s"));
    $new_user_model->setLastUpdated(date("Y:m:d H:i:s"));

    // if user id exists, update existing user, otherwise create a new one
    $update_id = ($old_user_model) ? $old_user_model->getId() : false;
    return $this->user_manager->createUpdateUser($new_user_model, $update_id);
  } // end saveUser

} // end controller
