<?php

require_once(APPLICATION_ROOT . "businesslogic" . DS . "UserManager.php");
require_once(APPLICATION_ROOT . "businesslogic" . DS . "Validator.php");

class UsersOverviewController extends Controller {

  private $user_manager;

  function __construct() {
    $this->user_manager = new UserManager();
  } // end constructor

  public function handle($template) {
    $user_model_session = $this->user_manager->getLoggedInUser($template);
    if (!$user_model_session) redirect("error_403", 403);

    $users = $this->user_manager->getUsers();
    $template->assign("users", $users);
  } // end handle

} // end controller
