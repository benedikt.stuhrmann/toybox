var gulp = require('gulp');
var sass = require('gulp-sass');

/* #### SCSS to CSS #### */
gulp.task('default', function() {
  //styles (.scss --> .css)
  gulp.src('./public/resources/css/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/resources/css'));

  //styles for modules (.scss --> .css)
  var fs = require('fs');
  var path = './application/modules';
  var filenames = fs.readdirSync(path);
  filenames.forEach(function (name) {
    if (name === "." || name === "..") return;
    if (fs.lstatSync(path + "/" + name).isDirectory()) {
      gulp.src('./application/modules/' + name + '/*.scss')
          .pipe(sass().on('error', sass.logError))
          .pipe(gulp.dest('./application/modules/' + name));
    }
  });
});