<?php
/* Smarty version 3.1.30, created on 2018-02-03 22:21:36
  from "D:\wamp64\www\toybox\application\views\home.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a7635f031a189_76273629',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f481efb357a228030742e3d8ef6df05448a21d5a' => 
    array (
      0 => 'D:\\wamp64\\www\\toybox\\application\\views\\home.html',
      1 => 1517696483,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a7635f031a189_76273629 (Smarty_Internal_Template $_smarty_tpl) {
?>
<header class="container-fuid">
  <div class="row"><img src="/toybox/resources/images/toybox_logo.svg" alt="Benes Toxboy" title="Benes Toybox"/></div>
  <div class="row">
    <nav>
      <ul class="nav">
        <li><a href="/toybox/">Home</a></li>
        <li><a href="/toybox/modules/">Modules</a></li>
      </ul>
    </nav>
  </div>
</header>
<main class="container"><h2>My private place to test fancy stuff</h2>
  <p>
    Hey, I'm Benedikt.
    And I'm using this place to test out all kinds of web related stuff I always wanted to test out.
    Wether it's testing stuff for my other projects or simply playing around with some features i newly discovered. </p>
</main>
<footer class="container-fluid">
  <div class="row"><p>Copyright - Benedikt Stuhrmann</p></div>
</footer><?php }
}
