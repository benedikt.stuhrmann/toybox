<?php

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", dirname(dirname(__FILE__)));
define("APPLICATION_ROOT", dirname(dirname(__FILE__)) . DS ."application" . DS);

require_once(APPLICATION_ROOT . "businesslogic" . DS . "Engine.php");
$engine = new Engine();
$engine->handle();