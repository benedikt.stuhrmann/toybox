$(document).ready(function(){

  addModulesListeners();

}); // end ready

function addModulesListeners () {
  // show create new module modal, when clicking on create new tile
  $(".tile.create-new").on("click", function() {
    $("#create-edit-module-modal").modal();
  });

  // auto-completes code of module, when module-url is changed
  $("#url").on("change", function() {
    if ($("#code").val() === "") {
      $("#code").val($("#url").val());
      format_code();
    }
  });

  // format module-code when value changes
  $("#code").on("change", function() {
    format_code();
  });

  // auto-complete short description with beginning of module-description
  $("#description").on("change", function() {
    if ($("#short-description").val() === "") {
      $("#short-description").val($("#description").val().substring(0, 400));
    }
  });

  // update char count for short-description
  $("#short-description").on("input", function() {
    $(".char-count .count").text($(this).val().length);
  });

  // on submit validate and submit form
  $("#create-edit-module-form").on("submit", function(e) {
    e.preventDefault();
    submitCreateEditModuleForm();
  });

  // update checked status of visibility switch
  $("#visibility label").on("click", function() {
    $("#visibility input[type='radio']").prop("checked", false);
    $(this).next("input[type='radio']").prop("checked", true);
  });

  $("#image-cropper").cropit({
    allowDragNDrop: false
  });

} // end addModulesListeners

function format_code () {
  let formatted_code = $("#code").val().trim(); // remove whitespaces from start and end
  formatted_code = formatted_code.split("-").join("_"); // replace - with _
  formatted_code = formatted_code.split(" ").join("_"); // replace " " with _
  formatted_code = custom_trim(formatted_code, "_"); // trim _ from start and end
  formatted_code = formatted_code.replace(/[`~!@#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, ""); // remove special chars except _
  $("#code").val(formatted_code);
} // end format_code

function custom_trim (string, char) {
  if (char === "]")  char = "\\]";
  if (char === "\\") char = "\\\\";
  return string.replace(new RegExp(
      "^[" + char + "]+|[" + char + "]+$", "g"
  ), "");
} // end custom_trim

function submitCreateEditModuleForm () {
  $("#create-edit-module-form .error, #create-edit-module-form .success").addClass("hidden");
  let form = $("#create-edit-module-form");

  form.parsley().validate();
  if (form.parsley().isValid()) {
    // thumbnail from image cropper
    let thumb = $("#image-cropper").cropit("export", {type: "image/png"});
    $("#exported-thumbnail").val(thumb);

    $.ajax({
      url : ajaxUrl("createEditModule"),
      method : "POST",
      dataType : "json",
      data : form.serialize()
    }).done(function(response) {
      // TODO: use response to display success/error
    });
  }
  return false;
} // end submitCreateEditModuleForm