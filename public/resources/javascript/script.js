$(document).ready(function() {

  highlightMainNavigation();
  highlightUserNavigation();
  addMainListeners();
  showUserMenu();

}); // end ready

function highlightMainNavigation() {
  // adds the "active"-class to the menu item in the navbar for the active site
  let nav_item = $("nav a[href='" + document.location.pathname + "']");
  if (nav_item.length) nav_item.addClass("active");
  else {
    // adds the current site to the main navigation
    let title = document.location.pathname.replace("/toybox/", "").split("/")[0].split("-"); // first element form url string (without the toybox)
    title = title[0].replace(/\b\w/g, l => l.toUpperCase()); // first letter to uppercase
    $("nav ul.nav").append("<li><a href='" + document.location.pathname + "' class='btn active'>" + title + "</a></li>");
  }
} // end highlightMainNavigation

function highlightUserNavigation() {
  // adds the "active"-class to the menu item in the user-menu for the active site
  let location = document.location.pathname.replace("/toybox/", "").split("/")[0];
  let nav_item = $("#user-menu li a[href*='" + location + "']");
  if (nav_item.length) {
    nav_item = nav_item.parent("li");
    nav_item.addClass("active");
  }
} // end highlightUserNavigation

function addMainListeners() {
  $("[data-toggle='tooltip']").tooltip({
    "delay": { show: 500 }
  });

  $("header").on("click", "#login", showLogin);

  $("#login-screen .close").on("click", function(){
    showLogin(false);
  });

  $("#login-form").on("submit", function(e) {
    e.preventDefault();
    login();
  });

  $("header").on("click", "#logout", function() {
    logout();
  });

  $("header").on("click", "#menu", function() {
    toggleUserMenu();
  });

  $(".one-back").on("click", function () {
    window.history.go(-1);
  });

  $(".two-back").on("click", function () {
    window.history.go(-2);
  });
} // end addMainListeners

/**
 * Modifies a given function name, so it gets recognized as an AJAX call
 * @param $function_name : name of the function (has to exist in AjaxHandler)
 * @returns string : URL that ajax call should go to, containing the function name
 */
function ajaxUrl($function_name) {
  return "/toybox/ajax_" + $function_name;
} // end ajaxUrl

function showUserMenu(show) {
  if (show || show == null) {
    // show
    $("#user-menu").removeClass("hidden");
    setTimeout(function () {
      $("#user-menu").addClass("show");
    }, 300);
  } else {
    // hide
    $("#user-menu").removeClass("show");
    setTimeout(function () {
      $("#user-menu").addClass("hidden");
    }, 300);
  }
} // end showUsersMenu

function showLogin(show) {
  if (show || show == null) {
    // show
    showUserMenu(false);
    $("#login-screen").removeClass("hidden");
    $("body").addClass("lock");
    setTimeout(function () {
      $("#login-screen").addClass("show");
      setTimeout(function () {
        $("#login-screen .content").removeClass("hidden");
        setTimeout(function () {
          $("#login-screen .content").addClass("show");
        }, 50);
      }, 1000);
    }, 50);

  } else {
    // hide
    $("#login-screen .content").removeClass("show");
    setTimeout(function () {
      $("#login-screen .content").addClass("hidden");
      $("#login-screen").removeClass("show");
      setTimeout(function () {
        $("#login-screen").addClass("hidden");
        $("#login-form").trigger("reset").parsley().reset();
        $("body").removeClass("lock");
        showUserMenu();
      }, 500);
    }, 1000);
  }
} // end showLogin

function login() {
  $("#login-form .error").addClass("hidden");
  let form = $("#login-form");
  form.parsley().validate();

  if (form.parsley().isValid()) {
    // make ajax call to log in the user
    $.ajax({
      url : ajaxUrl("login"),
      method : "POST",
      dataType : "json",
      data : {
        email :       $("#email-login").val(),
        password :    $("#password-login").val(),
        remember_me : $("#remember-me").prop("checked")
      }
    }).done(function(response) {
        if (response.answ === "success") {
          // login succeeded
          $("#user-menu").replaceWith(response.menu);
          showLogin(false);
        } else if (response.answ === "no-match") {
          // email or password is wrong
          $("#login-form .error").html("E-Mail oder Passwort falsch").removeClass("hidden");
        } else if (response.answ === "error") {
          // something went wrong
          $("#login-form .error").removeClass("hidden");
        }
    });
  }
  return false;
} // end login

function logout() {
  $.ajax({
    url : ajaxUrl("logout"),
    method : "POST",
    dataType : "json",
    data : {}
  }).done(function(response) {
    if (response.answ === "success") {
      // logout succeeded
      location.reload();
    } else if (response.answ === "error") {
      // something went wrong
      alert("something went wrong");
    }
  });
} // end logout

function toggleUserMenu() {
    if ($("#user-menu li").hasClass("show")) {
      // hide
      $("#user-menu li").removeClass("show");
      $("#user-menu li").addClass("hide");
    } else {
      // show
      $("#user-menu li").removeClass("hide");
      $("#user-menu li").addClass("show");
    }
} // end toggleUserMenu