$(document).ready(function() {

  addSettingsListeners();

}); // end ready

function addSettingsListeners() {

  // validate the create edit user form and submit it, if valid
  $("#create-edit-user-form").on("submit", function(e) {
    e.preventDefault();
    submitCreateEditUserForm();
  });

  // dynamically change required of password inputs
  $("#create-edit-user-form #password").on("change", function() {
    // if password is entered, make password repeat required
    let password_input = $("#create-edit-user-form #password");
    if (password_input.val() !== "") $("#create-edit-user-form #password, #create-edit-user-form #password-repeat").attr("required", "required");
    else if ($("#create-edit-user-form input[name='save']").val() !== "new") $("#create-edit-user-form #password, #create-edit-user-form #password-repeat").removeAttr("required", "required");
  });

} // end addSettingsListeners

function submitCreateEditUserForm() {
  $("#create-edit-user-form .error, #create-edit-user-form .success").addClass("hidden");
  let form = $("#create-edit-user-form");

  form.parsley().validate();
  if (form.parsley().isValid()) {
    $.ajax({
      url : ajaxUrl("createEditUser"),
      method : "POST",
      dataType : "json",
      data : form.serialize()
    }).done(function(response) {
      if (response.answ === "success") {
        // edit/creation succeeded
        $("#create-edit-user-form .success").removeClass("hidden");
      } else if (response.answ === "error") {
        // something went wrong
        $("#create-edit-user-form .error").removeClass("hidden");
      } else {
        let element;
        for(key in response.answ){
          element = null;
          switch (key) {
            case "username" : element = $("#create-edit-user-form #username").next(".error");
              break;
            case "firstname" : element = $("#create-edit-user-form #first-name").next(".error");
              break;
            case "lastname" : element = $("#create-edit-user-form #last-name").next(".error");
              break;
            case "email" : element = $("#create-edit-user-form #email").next(".error");
              break;
            case "email_repeat" : element = $("#create-edit-user-form #email-repeat").next(".error");
              break;
            case "password" : element = $("#create-edit-user-form #password").next(".error");
              break;
            case "password_repeat" : element = $("#create-edit-user-form #password-repeat").next(".error");
              break;
          }

          let message = "";
          Object.keys(response.answ[key]).forEach(function (constraint) {
            message += response.answ[key][constraint];
          });

          element.html(message).removeClass("hidden");
        }
      }
    });
  }
  return false;
} // end submitCreateEditUserForm